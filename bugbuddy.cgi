#!/usr/bin/perl -t

# DO NOT USE THIS!
#
# WARNING FROM bugmaster@gnome.org
# 
# This XML-RPC experimental. I do NOT want to see other products using this
# interface. If I find out I will remove your product from bugzilla.gnome.org.
#

use strict;

use lib qw(.);

use XMLRPC::Transport::HTTP;

use Bugzilla;
use Bugzilla::Constants;

use Bugzilla::Util;
use Bugzilla::Config;
use Bugzilla::Constants;
use Bugzilla::Error;
use Bugzilla::BugMail;
use Bugzilla::RPC;
require 'globals.pl';

use vars qw($userid);

use Bugzilla::Bug;

GetVersionTable();

Bugzilla->usage_mode(Bugzilla::Constants::USAGE_MODE_WEBSERVICE);

XMLRPC::Transport::HTTP::CGI
  -> dispatch_with({
  	'BugBuddy' => 'Bugzilla::RPC',
	})
  -> handle
;


