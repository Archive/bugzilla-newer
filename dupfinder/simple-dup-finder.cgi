#!/usr/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#see also bug 87927

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /dupfinder$/) {
        chdir "..";
    }
}
push @INC, "dupfinder/."; # many scripts now are in the reports subdirectory


use strict;

use vars qw($vars);

require "CGI.pl";

require "globals.pl";
require "find-traces.pl";

use Bugzilla;
use Bugzilla::GNOME;

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $template = Bugzilla->template;

#set up the page
GetVersionTable();

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
Bugzilla->login();

print $cgi->header();

#
# Obtain the relevant parameters
#
my $bug_id = $cgi->param('id');
my $comment_id = $cgi->param('comment_id');
my $limit_to_same_product = $cgi->param('same_product');
my $custom_description = $cgi->param('custom_description');
my $custom_functions = $cgi->param('custom_functions');
my $limit = $cgi->param('limit');
  if (!defined($limit)) {
    $limit = 10;
  }
  $vars->{'limit'} = $limit;
my $simpleform = $cgi->param('simpleform');
  if (defined($simpleform) && $simpleform =~ /true/i) {
    $vars->{'simpleform'} = 1;
  } else {
    $vars->{'simpleform'} = 0;
  }
my $max_text_length = 3000;

if (defined($bug_id) || defined($custom_description) || defined($custom_functions)) {
  #
  # First, get the text to search on
  #
  my $text;
  if (defined($custom_description)) {
    $text = $custom_description;
  }
  else {
    ValidateBugID($bug_id);
    $comment_id ||= 0;
    $comment_id = 0 unless detaint_natural($comment_id);
    
    $text = 
      $dbh->selectrow_array("SELECT substr(thetext, 1, 7500)
                               FROM longdescs
                              WHERE bug_id = ?
                           ORDER BY bug_when
                              LIMIT 1 OFFSET $comment_id", undef, ($bug_id));
    $vars->{'bug_id'} = $bug_id;
    $vars->{'comment_id'} = $comment_id;
  }

  #
  # Second, determine if the text contains a stack trace
  #
  my @functions = get_traces_from_string($text);
  my $has_stack_trace = scalar @functions;

  if ($has_stack_trace) {
    #
    # Show bugs that have the same first five important functions (usually
    # because it has the same stack trace)
    #
    my ($limited, $data) = show_duplicates_given_functions(@functions);
    $vars->{'limited'} = $limited;
    $vars->{'bugs'} = $data;
    $vars->{'functions'} = \@functions;

    print $cgi->header();
    $template->process("reports/dupfinder-easy-results.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
  } else {
    #
    # Do a natural language search on the text to determine which bugs most
    # closely match the text
    #
    $text = substr($text, 0, $max_text_length);
    my $fulltext_search =
      $dbh->sql_fulltext_search('longdescs.thetext', $dbh->quote($text));
    my $query = "SELECT DISTINCT bugs.bug_id,
                                 substr(thetext, 1, 5000) AS comment,
                                 bug_status AS status,
                                 resolution,
                                 products.name AS product,
                                 substr(short_desc, 1, 60) AS summary,
                                 $fulltext_search AS relevance
                            FROM bugs
                      INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id
                      INNER JOIN products  ON products.id = bugs.product_id
                           WHERE $fulltext_search";
    if (defined($bug_id)) {
      $query .= "            AND bugs.bug_id != $bug_id";
      if (defined($limit_to_same_product)) {
        my $product_id = $dbh->selectrow_array("SELECT product_id FROM bugs
                                                WHERE  bug_id = $bug_id");
        $query .= "          AND bugs.product_id = $product_id";
      }
    }
    $query .= "            LIMIT $limit";
    my $bugs = $dbh->selectall_arrayref($query, {'Slice' => {}});

    $vars->{'bugs'} = $bugs;
    $vars->{'search_text'} = $text;

    print $cgi->header();
    $template->process("reports/dupfinder-easy-results.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
  }
} else{
  #
  # No bug number or custom_description was specified for searching, so let's
  # as the user for it.
  #
  print $cgi->header();
  $template->process("reports/dupfinder-easy-select.html.tmpl", $vars)
    || ThrowTemplateError($template->error());
}
