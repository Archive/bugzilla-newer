# -*- Mode: perl; indent-tabs-mode: nil -*-
use strict;

use Bugzilla;
my $dbh = Bugzilla->dbh;


=head1 get_long_description_from_database

takes a bug number and returns the long description of the bug.

=cut

sub get_long_description_from_database {
  my ($bugnum) = @_;
  $bugnum =~ s/(\d+)/$1/;

    # Use the last comment with a stacktrace. That comment should hopefully
    # have the best one.
    my $query = "
          SELECT thetext
            FROM longdescs
           WHERE bug_id = ?
             AND isprivate = 0
             AND thetext REGEXP \"[^>][^ ]#[[:digit:]]+ +0x[0-9A-F]+ in \([[:alnum:]]+\)\"
        ORDER BY bug_when DESC " . $dbh->sql_limit(1);

    my $result = join("", @{$dbh->selectcol_arrayref($query, undef, $bugnum)});
    
    return $result;
}

# WordListQuery was modified from buglist.cgi (GetByWordListSubstr)
sub WordListQuery {
    my ($field, @strs) = (@_);
    my @list;

    foreach my $word (@strs) {
        if ($word ne "") {
            push(@list, "INSTR(LOWER($field), " . lc($dbh->quote($word)) . ")");
        }
    }

    return \@list;
}

=head1 show_duplicates_given_functions

takes a list of functions to be found in stack traces and returns
a list of bugs which match.

=cut
sub show_duplicates_given_functions {
my @functions = @_ ;

#Ben had been using a left join here, but it didn't work for some reason so I took it out. [Luis]
#We tried to originally judge order by just directly searching using REGEXP but mysql is sloooow
# for that case

  my $query = "    SELECT DISTINCT bugs.bug_id,
                          substring(bugs.bug_status,1,4)    AS status, 
                          substring(bugs.resolution,1,4)    AS resolution,
                          substring(products.name,1,12)     AS product,
                          substring(bugs.short_desc, 1, 60) AS summary,
                          duplicates.dupe_of                AS dupe_of
                     FROM bugs
               INNER JOIN longdescs
                       ON bugs.bug_id = longdescs.bug_id
               INNER JOIN products
                       ON bugs.product_id = products.id
                LEFT JOIN duplicates
                       ON bugs.bug_id = duplicates.dupe
                    WHERE longdescs.isprivate = 0 
                      AND ";

  ## The old, slow way which doesn't make use of mysql's fulltext search
  ## abilities
  # $query .= join(" AND ", @{WordListQuery("longdescs.thetext", @functions)});

  ## The better, rockingly fast way that uses mysql's fulltext search abilities
  $query .= "MATCH (longdescs.thetext)
             AGAINST (" . $dbh->quote("+" . join(" +", @functions)) . " 
               IN BOOLEAN MODE) ";

  # Group by id and limit to 100 (if we end up getting just one function that
  # appears in all bugs, then the list will be uselessly ridiculously long
  # and put a heavy load on the server too.  Lists become uselessly long far
  # before 100 anyway...
  $query .= $dbh->sql_limit(100);

  my $data = $dbh->selectall_arrayref($query, {'Slice' => {}});

  return (scalar(@$data) == 100, $data);
}

#this is total crack, but it makes the perl+apache stuff happy- don't delete
1;
