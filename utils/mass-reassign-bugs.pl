#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# Description
#   Changes all bugs in <product> with the specified <field> being equal
#   to <old-value>.  The value of <field> will be changed to <new-value>
#   and <changer> will be recorded as the bugzilla user who made the change.
#
#   Thoroughly checks all data, summarizes the changes that will be made,
#   and verifies that the changes are correct before making them.
#
# Usage:
#   ./mass-reassign-bugs.pl -p <product> -f <field> -o <old-value> \
#        -n <new-value> -c <changer>
#
# Example:
#   ./mass-reassign-bugs.pl -p gedit -f assigned_to
#        -o paolo@gnome.org -n gedit-maint@gnome.bugs \
#        -c newren@gmail.com
#
# Notes:
#   <field> currently must be one of
#     assigned_to
#     qa_contact
#     version
#   <old-value> and <new-value> must be valid values for <field>
#   <changer> must be a valid bugzilla login

use strict;

# Bugzilla doesn't expect us to be running in the utils subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /utils$/) {
        chdir "..";
    }
}
use lib ".";

require "globals.pl";
use Bugzilla;
my $dbh = Bugzilla->dbh;
use Getopt::Long;

sub sanity_check_field_values
{
  my ($product_id, $field, $old, $new) = @_;
  my ($old_db_value, $new_db_value, $old_quoted_value, $new_quoted_value);

  if ($field eq "assigned_to" ||
      $field eq "qa_contact") {
    $old_db_value = 
      $dbh->selectrow_array("SELECT userid FROM profiles WHERE login_name = ?",
                            undef, $old);
    die "Invalid old $field: $old" unless (defined($old_db_value));
    $new_db_value = 
      $dbh->selectrow_array("SELECT userid FROM profiles WHERE login_name = ?",
                            undef, $new);
    die "Invalid new $field: $new" unless (defined($new_db_value));
  } elsif ($field eq "version") {
    $old_db_value = 
      $dbh->selectrow_array("SELECT value FROM versions " .
                            "WHERE product_id = ? AND value = ?",
                            undef, $product_id, $old);
    die "Invalid old $field: $old" unless (defined($old_db_value));
    $new_db_value = 
      $dbh->selectrow_array("SELECT value FROM versions " .
                            "WHERE product_id = ? AND value = ?",
                            undef, $product_id, $new);
    die "Invalid new $field: $new" unless (defined($new_db_value));
  } else {
    die "Invalid field: $field";
  }

  $old_quoted_value = $dbh->quote($old);
  $new_quoted_value = $dbh->quote($new);

  return ($old_db_value, $new_db_value, $old_quoted_value, $new_quoted_value);
}

  # Get any parameters passed
  my ($help, $product, $component, $field, $old, $new, $changer) =
    (0, undef, '%', undef, undef, undef, undef);
  Getopt::Long::Configure ('bundling');
  # Recall that '=s' means string, '=i' would mean numeric, while nothing
  # means a simple flags
  GetOptions ('h|help|?'         => \$help,
              'p|product=s'      => \$product,
              't|component=s'    => \$component,
              'f|field=s'        => \$field,
              'o|old-value=s'    => \$old,
              'n|new-value=s'    => \$new,
              'c|changer=s'      => \$changer);
  # Freakin' long help...
  if ($help || 
      !defined($product) ||
      !defined($component) ||
      !defined($field)   ||
      !defined($old)     ||
      !defined($new)     ||
      !defined($changer)) {
      print "Usage:\n" .
            "  $0 --product=<product> --field=<field>\n" .
            "     --old-value=<old-value> --new-value=<new-value>\n" .
            "     --changer=<changer>\n" .
            "\n" .
            "Modifies all bugs in <product> that currently have\n" .
            "<old-value> as the value of <field>.  Sets the value of\n" .
            "<field> to <new-value>, and records the change as having been\n" .
            "done by <changer>.  All long arguments have a corresponding\n" .
            "short form using the first letter.\n";
      exit 1;
  }

  #
  # Validate the data passed
  #
  trick_taint($product);
  trick_taint($component);
  trick_taint($field);
  trick_taint($old);
  trick_taint($new);
  trick_taint($changer);
  my $product_id = 
    $dbh->selectrow_array("SELECT id FROM products WHERE name = ?",
                          undef, $product);
  die "Invalid product: $product" unless (defined($product_id));
  my ($old_db_value, $new_db_value, $old_quoted_value, $new_quoted_value) =
    sanity_check_field_values($product_id, $field, $old, $new);
  my $changer_id = 
    $dbh->selectrow_array("SELECT userid FROM profiles WHERE login_name = ?",
                          undef, $changer);
  die "Invalid changer: $changer" unless (defined($changer_id));
  my $num_bugs = $dbh->selectrow_array(
         "SELECT COUNT(bug_id)
            FROM bugs
      INNER JOIN components
              ON bugs.component_id = components.id
           WHERE bugs.product_id = ?
             AND $field = ?
             AND components.name LIKE ?",
                 undef, ($product_id, $old_db_value, $component));

  if ($num_bugs eq 0) {
    print "No bugs would be affected by this change!\n";
    exit 1;
  }

  #
  # Verify that the user really wants to make the change
  #
  print "Reassigning all bugs in\n" .
        "  $product, component: $component\n" .
        "with $field equal to\n" .
        "  $old\n" .
        "to now be equal to\n" .
        "  $new\n" .
        "and recording\n" .
        "  $changer\n" .
        "as the user who made the change in the bug activity.\n" .
        "which will affect $num_bugs bugs.\n" .
        "Is this correct? (Y/N): ";
  my $answer = <STDIN>;
  if ($answer !~ /^\s*[Yy]/) {
      $| = 1;  # Force stdout to flush immediately
      print "\nTough, making the change anyway!";
      sleep(1);
      print "...just kidding!\n";
      print "Script aborted.\n";
      exit();
  }

  #
  # Find the fieldid and current time for the bugs_activity changes
  #
  my $fieldid =
    $dbh->selectrow_array("SELECT fieldid FROM fielddefs WHERE name = ?",
                          undef, $field);
  my $curtime = $dbh->selectrow_array("SELECT NOW()");

  #
  # Prepare common SQL commands and lock the relevant tables
  #
  my $change_bug_sth =
    $dbh->prepare("UPDATE bugs
                   SET $field = ?, delta_ts = delta_ts,
                       lastdiffed = '$curtime'
                   WHERE $field = ? AND bug_id = ?");
  my $insert_activity_entry_sth =
    $dbh->prepare("INSERT INTO bugs_activity
                   VALUES (?, $changer_id, '$curtime', $fieldid, 
                     $old_quoted_value, $new_quoted_value, NULL)");
  $dbh->bz_lock_tables('bugs WRITE', 'bugs_activity WRITE', 'components READ');

  #
  # Find the relevant bugs and update them
  #
  my $bug_list = $dbh->selectcol_arrayref(
          "SELECT bug_id
            FROM bugs
      INNER JOIN components
              ON bugs.component_id = components.id
           WHERE bugs.product_id = ?
             AND $field = ?
             AND components.name LIKE ?",
                 undef, ($product_id, $old_db_value, $component));

  foreach my $bug (@$bug_list) {
    $change_bug_sth->execute($new_db_value, $old_db_value, $bug);
    $insert_activity_entry_sth->execute($bug);
  }

  #
  # Unlock the relevant tables
  #
  $dbh->bz_unlock_tables();

  print "Done reassigning bugs in $product from $field being\n" .
        "  $old\n" .
        "to now being\n" .
        "  $new\n";
