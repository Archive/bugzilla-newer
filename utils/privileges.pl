#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#

use strict;

# Bugzilla doesn't expect us to be running in the utils subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /utils$/) {
        chdir "..";
    }
}
use lib ".";

require "globals.pl";

use Bugzilla;
use Bugzilla::User;

$| = 1; # unbuffered STDOUT for nice progress info

my $dbh = Bugzilla->dbh;
my $sth;
my @row;

my $email = $ARGV[0];

if (!defined($email) )
{
        print "Usage: privileges email-address\n";
        exit();
}

my $error = "";

my $user = Bugzilla::User->new_from_login($email);

my $userid = $user->id;
my $realname = $user->name;

if (!$userid) {
    $error .= "Email address does not exist\n";
}

if ($error) {
    print $error;
    exit(1)
}

$sth = $dbh->prepare(
    'SELECT login_name, profiles_when, CONCAT("-", oldvalue), CONCAT("+",newvalue)
       FROM profiles INNER JOIN profiles_activity
         ON profiles.userid = profiles_activity.who
      WHERE profiles_activity.userid = ?');

$sth->execute($userid);

print "-- Persons giving priviledges to $email\n";
while (@row = $sth->fetchrow_array) {
    print join("\t", @row) . "\n"
}


$sth = $dbh->prepare(
    'SELECT login_name, profiles_when, oldvalue, newvalue
       FROM profiles INNER JOIN profiles_activity
         ON profiles.userid = profiles_activity.userid
      WHERE profiles_activity.who = ?');

$sth->execute($userid);

print "-- Priviledges given out by $email\n";
while (@row = $sth->fetchrow_array) {
    print join("\t", @row) . "\n"
}
