#!/usr/bin/perl -w

use strict;
use DBI;
use Encode;

require "globals.pl";

my $dbh = Bugzilla->dbh;


do 'localconfig';

# Only do it if parameter -doit given.
my $doit = grep(/^-doit$/, @ARGV);

# Current data encoding.
my $encoding_from = 'iso-8859-1';
# Encoding to convert to.
my $encoding_to   = 'utf-8';

my $counter = 0;

my %conv = (
   'attachments'       => {'key'    => ['attach_id'],
                           'fields' => ['description', 'filename']},
#   'bugs'              => {'key'    => ['bug_id'],
#                           'fields' => ['short_desc', 'keywords', 'version',
#                                        'status_whiteboard', 'target_milestone',
#                                        'alias']},
#   'bugs_activity'     => {'key'    => ['bug_id', 'who', 'bug_when', 'fieldid'],
#                           'fields' => ['added', 'removed']},
#   'classifications'   => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'components'        => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'fielddefs'         => {'key'    => ['fieldid'],
#                           'fields' => ['description']},
#   'flagtypes'         => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'groups'            => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'keyworddefs'       => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'longdescs'         => {'key'    => ['bug_id', 'who', 'bug_when', 'thetext'],
#                           'fields' => ['thetext']},
#   'milestones'        => {'key'    => ['value', 'product_id'],
#                           'fields' => ['value']},
   'namedqueries'      => {'key'    => ['userid', 'name'],
                           'fields' => ['name']},
#   'products'          => {'key'    => ['id'],
#                           'fields' => ['name', 'description']},
#   'profiles'          => {'key'    => ['userid'],
#                           'fields' => ['realname', 'disabledtext']},
#   'profiles_activity' => {'key'    => ['userid', 'profiles_when', 'fieldid'],
#                           'fields' => ['oldvalue', 'newvalue']},
#   'quips'             => {'key'    => ['quipid'],
#                           'fields' => ['quip']},
#   'series'            => {'key'    => ['series_id'],
#                           'fields' => ['name']},
#   'series_categories' => {'key'    => ['id'],
#                           'fields' => ['name']},
#   'versions'          => {'key'    => ['value', 'product_id'],
#                           'fields' => ['value']},
#   'whine_events'      => {'key'    => ['id'],
#                           'fields' => ['subject', 'body']},
#   'whine_queries'     => {'key'    => ['id'],
#                           'fields' => ['query_name', 'title']},
);

$dbh->do('LOCK TABLES '.join(' WRITE, ', keys(%conv)).' WRITE');
foreach my $table (keys(%conv)) {
   print "\nConverting field(s) ".join(', ', @{$conv{$table}{'fields'}}).
         " of table $table...";
   my $shortkey = $conv{$table}{'key'}[0];
   my $selsth = $dbh->prepare(
      'SELECT '.
      join(',', @{$conv{$table}{'key'}}, @{$conv{$table}{'fields'}}).
      " FROM $table ORDER BY $shortkey");
   my $updsth = $dbh->prepare(
      "UPDATE $table SET ".
      join('=?, ', sort(@{$conv{$table}{'fields'}})).'=? WHERE '.
      join('=? AND ', sort(@{$conv{$table}{'key'}})).'=?');
   $selsth->execute();
   while (my $line = $selsth->fetchrow_hashref()) {
      ++$counter % 1000 || print '.';
      my @newvaluearray;
      my @keyarray;
      my $change = 0;
      foreach my $field (sort(@{$conv{$table}{'fields'}})) {
         my $buf = $$line{$field};
         $change = 1 if ($buf && !decode_utf8($buf));
         defined($buf)
            && !defined(Encode::from_to($buf, $encoding_from, $encoding_to))
            && die $buf;
         push(@newvaluearray, $buf);
      }
      foreach my $field (sort(@{$conv{$table}{'key'}})) {
         push(@keyarray, $$line{$field});
      }
      $doit && $change && $updsth->execute(@newvaluearray, @keyarray);
   }
}
print "\n";
