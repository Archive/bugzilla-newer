#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

use strict;

# Bugzilla doesn't expect us to be running in the utils subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /utils$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # import help libs are in the reports subdirectory
use lib ".";

require "globals.pl";
require "weekly-summary-utils.pl";
require "recent-mostfrequent-utils.pl";
require "product-activity-utils.pl";

use Bugzilla;
use Bugzilla::User;
my $dbh = Bugzilla->dbh;
use Getopt::Long;

# Get any parameters passed
my ($help, $days, $force) = (0, undef, 0);
Getopt::Long::Configure ('bundling');
GetOptions ('h|help|?'  => \$help,
            'd|days=s'  => \$days,
            'f|force' => \$force);
if ($help) {
    print "Please WTFM and then R it.  Aren't I helpful?  ;-)\n";
    exit 1;
}
if (!defined($days)) {
    $days = 365;
}
$days =~ /[0-9]+/ || die "days parameter must be numeric\n";
trick_taint($days);

# First, make sure this is being run on the first 30 minutes of the new year
my $start_date = 
  $dbh->selectrow_array("SELECT DATE_SUB(CURDATE(), INTERVAL $days DAY)");
my $time = $dbh->selectrow_array("SELECT NOW()");
$time =~ /([0-9]+)/;
my $year = $1;
my $beg_allowed_time = $dbh->quote("$year-01-01 00:00:00");
my $end_allowed_time = $dbh->quote("$year-01-01 00:30:00");
my $too_soon = $dbh->selectrow_array("SELECT '$time' < $beg_allowed_time");
my $too_late = $dbh->selectrow_array("SELECT '$time' > $end_allowed_time");
if (($too_soon || $too_late) && !$force) {
    print "This script should be run in the first 30 minutes of " .
          "the new year.\n";
    exit 1;
}

print "Gathering stats for the last $days days " .
      "(plus whatever has happened today)...\n";

#
# Get the stats off the weekly bug summary
#
my ($products, $hunters, $reporters, $patchers, $reviewers)
  = (1000, 250, 250, 100, 100);
my $keyword = undef;
my $version = undef;
my $links = 'no';

print "Getting total bugs...";
my $totalbugs = &get_total_bugs_on_bugzilla($keyword, $version);
print "done.\n";

print "Getting number of bugs opened...";
my ($bugs_opened, $opened_buglist) = &bugs_opened($days, $keyword, 
                                                  $version);
print "done.\n";

print "Getting number of bugs closed...";
my ($bugs_closed, $closed_buglist) = &bugs_closed($days, $keyword,
                                                  $version);
print "done.\n";

print "Getting number of bugs closed that were marked as duplicates...";
my $num_dupes = 
  $dbh->selectrow_array("    SELECT COUNT(DISTINCT bugs.bug_id)
                               FROM bugs
                         INNER JOIN bugs_activity
                                 ON bugs.bug_id = bugs_activity.bug_id
                              WHERE bugs.bug_status
                                    IN ('RESOLVED', 'CLOSED', 'VERIFIED')
                                AND bugs.resolution='DUPLICATE'
                                AND bugs_activity.added='RESOLVED'
                                AND bugs_activity.bug_when >= $start_date");
print "done.\n";

print "Getting bug counts by product...";
my ($productlist) =
    &get_product_bug_lists($products, $days, $keyword, $links, $version);
print "done.\n";

print "Getting top triager list...";
my ($hunterlist) = 
    &get_bug_hunters_list($hunters, $days, $keyword, $links, $version);
print "done.\n";

print "Getting top reporter list...";
my ($reporterlist) =
    &get_bug_reporters_list($reporters, $days, $keyword, $links, $version);
print "done.\n";

print "Getting top patch submitter list...";
my ($patchsubmitterlist) = 
    &get_patch_submitters_list($patchers, $days, $keyword, $links, $version);
print "done.\n";

print "Getting top patch reviewer list...";
my ($patchreviewerlist) = 
    &get_patch_reviewers_list($reviewers, $days, $keyword, $links, $version);
print "done.\n";

print "Getting top duplicated bugs list...";
my ($duplicatelist) =
    &get_recent_mostfrequent_table($days);
print "done.\n";

print "Getting patch diligence ratings...";
my $patch_diligence_ratings =
    &get_patch_diligence_ratings(0, $days);
print "done.\n";

print "Getting bug responsiveness ratings...";
my $bug_responsiveness_ratings =
    &get_responsiveness_ratings(0, $days);
print "done.\n";


print "\n";
my ($name, $total, $opened, $change, $user, 
    $closed, $reported, $submitted, $reviewed);
mkdir "utils/stats";
#
# Saving general stats
#
print "Saving stat-overview.html...";
open(STATFILE, ">utils/stats/stat-overview.html")
  || die "Couldn't open file for writing.\n";
printf STATFILE
  "<p>\n" .
  "Here are some stats from bugzilla.gnome.org for the year %d.  Please\n" .
  "keep in mind that you should be careful to not draw too many\n" .
  "conclusions from these values[1].\n" .
  "</p>\n\n",
  $year-1;

printf STATFILE
  "<p>\n" .
  "Overall statistics:\n" .
  "<ul>\n" .
  "  <table>\n" .
  "    <tr><td>Current open reports:</td> <td>%d (*)</td></tr>\n" .
  "    <tr><td>Opened in %d:</td>         <td>%d</td></tr>\n" .
  "    <tr><td>Closed in %d:</td>         <td>%d</td></tr>\n" .
  "  </table>\n" .
  "  <p>Of the bugs closed, %d were marked as duplicates</p>\n" .
  "  <table>\n" .
  "    <tr><td>Patches submitted:</td> <td>%d</td></tr>\n" .
  "    <tr><td>Number of those marked as reviewed:</td> " .
  "        <td>%d (%.2f)</td></tr>\n" .
  "  </table>\n" .
  "  <p>(*): Excludes reports marked as enhancements</p>\n" .
  "</ul>\n",
  $totalbugs, $year-1, $bugs_opened, $year-1, $bugs_closed, $num_dupes,
  0, 0, 0;

print STATFILE
  "<p>\n" .
  "More detailed statistics can be found at:\n" .
  "<ul>\n" .
  "  <li><a href=\"top-triagers.txt\">top triagers</a></li>\n" .
  "  <li><a href=\"top-reporters.txt\">top reporters</a></li>\n" .
  "  <li><a href=\"top-patchers.txt\">top patch submitters</a></li>\n" .
  "  <li><a href=\"top-reviewers.txt\">top patch reviewers</a></li>\n" .
  "  <li><a href=\"top-duplicates.txt\">most duplicated bugs</a></li>\n" .
  "  <li><a href=\"products-bug-count.txt\">products with the most bug reports</a></li>\n" .
  "  <li><a href=\"products-patch-diligence.txt\">products best at patch review</a></li>\n" .
  "  <li><a href=\"products-bug-responsiveness.txt\">products best at responding to bugs</a></li>\n" .
  "</ul>\n" .
  "</p>\n";

print STATFILE
  "<p>\n" .
  "[1] Lots of work can be done by someone to improve Gnome, even in a way\n" .
  "that is related to work going on in bugzilla, but that doesn't show up\n" .
  "in the bugzilla stats.  Also, some of the stats don't necessarily mean\n" .
  "what you might think.  Perhaps some examples would help illustrate:  A\n" .
  "bug could be fixed without it ever being mentioned in bugzilla.\n" .
  "Maintainers will often fix bugs without doing the extra work of putting\n" .
  "the patch in bugzilla (it's often an unneeded extra step that would\n" .
  "just keep them from moving on and fixing other stuff).  'Number of bugs\n" .
  "closed' by no means implies number of bugs fixed; there have been at\n" .
  "least a few people in the bugsquad that closed several hundred (or even\n" .
  "a few thousand) bugs before actually fixing one.  Some people may\n" .
  "submit several patches as they are working on an issue for testing or\n" .
  "feedback reasons while others might wait until they have a more final\n" .
  "product before submitting.  And of course, bugs and patches are nowhere\n" .
  "close to having been born equal.\n" .
  "</p>\n";
close(STATFILE);
print "done.\n";


#
# Saving product stats
#
print "Saving products-bug-count stats...";
open(STATFILE, ">utils/stats/products-bug-count.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "Products with the most reports.  The last three columns note changes\n" .
  "in the last two years, while the total column shows how many bugs were\n" .
  "still open at the end of %d.  (This table is a little strange,\n" .
  "because Total excludes enhancement requests while other columns don't,\n" .
  "and because both Total and Closed exclude NEEDINFO bugs while Opened\n" .
  "does not.)\n" .
  "\n" .
  "Product               Total Opened Closed Change\n",
  $year-1;

my($prod_id, $opened_list, $closed_list);

foreach my $row (@$productlist) {
    ($prod_id, $name, $total, $opened, $opened_list, $closed, $closed_list, $change) = @$row;
    printf STATFILE "%-21s %5d %6s %6s %6d\n",
      $name, $total, '+' . $opened, '-' . $closed, $change;
}
close(STATFILE);
print "done.\n";


#
# Saving triager stats
#
print "Saving top-triagers stats...";
open(STATFILE, ">utils/stats/top-triagers.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "Some of the top %d bug closers from %d:\n" .
  "\n" .
  "# closed  Bugzilla User\n",
  $hunters, $year-1;

foreach my $row (@$hunterlist) {
    $closed   = $row->[1];
    $user = $row->[2]->identity;
    printf STATFILE "  %4d    %-30s\n", $closed, $user;
}
close(STATFILE);
print "done.\n";


#
# Saving reporter stats
#
print "Saving top-reporters stats...";
open(STATFILE, ">utils/stats/top-reporters.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "The top %d bug reporters from %d\n" .
  "\n" .
  "# reported  Bugzilla User\n",
  $reporters, $year-1;

foreach my $row (@$reporterlist) {
    $reported = $row->[1];
    $user     = $row->[2]->identity;
    printf STATFILE "  %4d      %-30s\n", $reported, $user;
}
close(STATFILE);
print "done.\n";


#
# Saving patcher stats
#
print "Saving top-patchers stats...";
open(STATFILE, ">utils/stats/top-patchers.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "The top %d patch submitters from %d\n" .
  "\n" .
  "# submitted  Bugzilla User\n",
  $patchers, $year-1;

foreach my $row (@$patchsubmitterlist) {
    $submitted = $row->[1];
    $user      = $row->[2]->identity;
    printf STATFILE "  %4d       %-30s\n", $submitted, $user;
}
close(STATFILE);
print "done.\n";


#
# Saving reviewer stats
#
print "Saving top-reviewers stats...";
open(STATFILE, ">utils/stats/top-reviewers.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "The top %d patch reviewers from %d\n" .
  "\n" .
  "# reviewed  Bugzilla User\n",
  $reviewers, $year-1;

foreach my $row (@$patchreviewerlist) {
    $reviewed = $row->[1];
    $user     = $row->[2]->identity;
    printf STATFILE "  %4d      %-30s\n", $reviewed, $user;
}
close(STATFILE);
print "done.\n";


#
# Saving duplicate stats
#
print "Saving top-duplicates stats...";
open(STATFILE, ">utils/stats/top-duplicates.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "Here is a list of bugs that have had 10 or more bugs marked as a\n" .
  "duplicate of it during %d\n" .
  "\n" .
  " Bug#  Dupes Status      Description\n",
  $year-1;

my $sth = $dbh->prepare("SELECT bug_status, substr(short_desc, 1, 54)
                           FROM bugs
                          WHERE bug_id = ?");
foreach my $row (sort {$b->[1] <=> $a->[1]} @$duplicatelist) {
    my $bug   = $row->[0];
    my $dupes = $row->[1];
    if ($dupes < 10) {
        last;
    }
    $sth->execute($bug);
    my ($status, $summary) = $sth->fetchrow_array;
    printf STATFILE "%6d  %3d  %-11s %s\n", $bug, $dupes, $status, $summary;
}
close(STATFILE);
print "done.\n";


#
# Saving patch diligence stats
#
print "Saving products-patch-diligence stats...";
open(STATFILE, ">utils/stats/products-patch-diligence.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "This is a list of Gnome Bugzilla products with the percentage of recent\n" .
  "patches filed in Bugzilla that have been reviewed for each product.\n" .
  "Only patches submitted during %d and before the last week of %d are\n" .
  "considered, and only products which have at least 4 patches submitted\n" .
  "in that time are shown.\n" .
  "\n" .
  "NOTE: A patch is considered to have been reviewed if it (a) has any\n" .
  "status set (except obsolete; obsolete patches are ignored in this\n" .
  "report), or (b) is in a bug marked closed or resolved.\n" .
  "\n" .
  "Product                        Reviewed Patch %%\n",
  $year-1, $year-1;

my @sorted_products = 
  (sort {$patch_diligence_ratings->{$b}->{rating} <=>
         $patch_diligence_ratings->{$a}->{rating} ||
         $patch_diligence_ratings->{$b}->{total} <=>
         $patch_diligence_ratings->{$a}->{total}}
   (keys %$patch_diligence_ratings));
foreach my $product (@sorted_products) {
    if ($patch_diligence_ratings->{$product}->{total} >= 4) {
        printf STATFILE "%-30s %3.0f (%4d/%4d)\n",
          substr($product, 0, 30),
          $patch_diligence_ratings->{$product}->{rating},
          $patch_diligence_ratings->{$product}->{reviewed},
          $patch_diligence_ratings->{$product}->{total};
    }
}
close(STATFILE);
print "done.\n";


#
# Saving bug reponsiveness stats
#
print "Saving products-bug-responsiveness stats...";
open(STATFILE, ">utils/stats/products-bug-responsiveness.txt")
  || die "Couldn't open file for writing.\n";

printf STATFILE
  "This is a list of Gnome Bugzilla products with the percentage of recent\n" .
  "bugs filed in Bugzilla that have been responded to for each product.\n" .
  "Only bugs filed during %d and before the last week of %d are\n" .
  "considered.\n" .
  "\n" .
  "Product                        Responded %%\n",
  $year-1, $year-1;

@sorted_products = 
  (sort {$bug_responsiveness_ratings->{$b}->{rating} <=>
         $bug_responsiveness_ratings->{$a}->{rating} ||
         $bug_responsiveness_ratings->{$b}->{total} <=>
         $bug_responsiveness_ratings->{$a}->{total}}
   (keys %$bug_responsiveness_ratings));
foreach my $product (@sorted_products) {
    printf STATFILE "%-30s %3.0f (%4d/%4d)\n",
      substr($product, 0, 30),
      $bug_responsiveness_ratings->{$product}->{rating},
      $bug_responsiveness_ratings->{$product}->{responded},
      $bug_responsiveness_ratings->{$product}->{total};
}
close(STATFILE);
print "done.\n";
