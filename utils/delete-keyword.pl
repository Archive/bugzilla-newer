#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

###########################################################################
# Global definitions
###########################################################################

use strict;

# Bugzilla doesn't expect us to be running in the utils subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /utils$/) {
        chdir "..";
    }
}
use lib ".";

require "globals.pl";
use vars qw($template $vars);

GetVersionTable();

my $dbh = Bugzilla->dbh;
my $sth;

my $name;
my $newkeywords;
foreach $name (@ARGV) {

my @row;
my @sql_data = ();

my $k_id = GetKeywordIdFromName($name)
 || die "No such keyword $name, aborting at this point";

trick_taint($name);
$sth = $dbh->prepare("SELECT bugs.bug_id, bugs.keywords " .
                     "FROM   bugs,keywords,keyworddefs " .
                     "WHERE  keywords.bug_id=bugs.bug_id " .
                     "  AND  keyworddefs.id=keywords.keywordid " .
                     "  AND  keyworddefs.name=?");
$sth->execute($name);

while ( @row = $sth->fetchrow_array ) {
        $newkeywords = $row[1];
        
        $newkeywords =~ s/(.*), \Q$name\E, (.*)/$1, $2/;
        $newkeywords =~ s/(.*), \Q$name\E/$1/;
        $newkeywords =~ s/\Q$name\E, (.*)/$1/;
        if ($newkeywords eq $name) {
                $newkeywords = "";
        }

        push (@sql_data, { keywords => $newkeywords, id => $row[0] });
}

if (scalar(@sql_data)) {
    $sth = $dbh->prepare("UPDATE bugs SET delta_ts = delta_ts, keywords=? WHERE bug_id=?");
    foreach my $row (@sql_data) {
        $sth->execute($row->{keywords}, $row->{id});
    }
}

$dbh->do("DELETE FROM keywords WHERE keywordid=?", undef, $k_id);
$dbh->do("DELETE FROM keyworddefs WHERE id=?", undef, $k_id);

print "Keyword $name ($k_id) deleted successfully.\n";
}
