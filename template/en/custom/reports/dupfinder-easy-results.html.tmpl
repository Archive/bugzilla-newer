[%# 1.0@bugzilla.org %]
[%# -*- mode: html -*- %]
[%# The contents of this file are subject to the Mozilla Public
  # License Version 1.1 (the "License"); you may not use this file
  # except in compliance with the License. You may obtain a copy of
  # the License at http://www.mozilla.org/MPL/
  #
  # Software distributed under the License is distributed on an "AS
  # IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
  # implied. See the License for the specific language governing
  # rights and limitations under the License.
  #
  # The Original Code is the Bugzilla Bug Tracking System.
  #
  # The Initial Developer of the Original Code is Olav Vitters
  # Corporation. Portions created by Netscape are
  # Copyright (C) 1998 Olav Vitters Corporation. All
  # Rights Reserved.
  #
  #%]

[%# INTERFACE:
  # This template has no interface.
  #%]

[% PROCESS global/variables.none.tmpl %]

[% PROCESS global/header.html.tmpl
    title = "Possible dupes"
    h1 = "" %]

<h1><center>Simple Bug Search</center></h1>

[% IF functions %]
  <p>
    A stack trace was found in the bug description you specified.  It appears
    that the first few relevant function calls in the stacktrace are:
  </p>
  <ol>
  [% FOREACH function = functions %]
    <li>[% function FILTER html %]</li>
  [% END %]
  </ol>

  <p>
    The following [% terms.bug %] reports [% IF limited %] (only the
    first 100 are shown)[% END %] contain all [% functions.size %]
    function calls and likely match the bug description you gave[% IF
    bug_id %].  Note that bug [% bug_id %], the bug you are searching for
    duplicates of, should also appear in this list[% END %]:
  </p>

  <table border=0 cellspacing=0 cellpadding=5>
  <tr>
      <td>Bug #</td>
      <td>Status</td>
      <td>Resolution</td>
      <td>Product</td>
      <td>Summary</td>
  </tr>
  [% FOREACH row = bugs %]
  <tr>
    <td align="right">
      <a href="show_bug.cgi?id=[% row.bug_id FILTER url_quote %]#stacktrace">
      [% row.bug_id FILTER html %]</a>
    </td>
    <td>[% row.status     FILTER html %]</td>
    <td>[% row.resolution FILTER html %]
        [% IF row.dupe_of %]
          (of <a href=
           "show_bug.cgi?id=[% row.dupe_of FILTER url_quote %]#stacktrace">
           [% row.dupe_of FILTER html %]</a>)
        [% END %]
    </td>
    <td>[% row.product FILTER html %]</td>
    <td>[% row.summary FILTER html %]</td>
  </tr>
  [% END %]
  </table>

  <p>
    You can <a href="query.cgi?query=[% "meta-status:all \"" 
    FILTER url_quote %][% functions.join('" "')
    FILTER url_quote %][% "\"" FILTER url_quote %]"> modify or refine this
    stacktrace search</a>.
  </p>
[% ELSE %]
  <p>
  The [% limit %] closest matches 
    [% IF bug_id %]
      to bug [% bug_id %]
      [% IF comment_id != 0 %]
        comment [% comment_id %]
      [% END %]
    [% END %]
  are shown below, in both a summarized list as well as an extended
  one.  The second list contains the actual comment that triggered the
  potential match.  The search text you entered is listed at the end.
  </p>

  <p>
    [% count = 0 %]
    <table>
    <tr>
      <th colspan=6 align="left">
        Summarized list
      </th>
    </tr>
    <tr>
      <td>Product</td>
      <td>Bug #</td>
      <td>Relevance</td>
      <td>Status</td>
      <td>Resolution</td>
      <td>Summary</td>
    </tr>
    [% FOREACH row = bugs %]
    <tr>
      <td>[% row.product %]</td>
      <td>[% row.bug_id %]</td>
      <td>[% row.relevance FILTER format('%4g') %]</td>
      <td>[% row.status %]</td>
      <td>[% row.resolution %]</td>
      <td>[% row.summary %]</td>
      [% count = count + 1 %]
    </tr>
    [% END %]
    </table>
  </p>

  <p>
    [% count = 0 %]
    <table>
    <tr>
      <th colspan=6 align="left">
        Long list
      </th>
    </tr>
    [% FOREACH row = bugs %]
      [% count = count + 1 %]
      <tr>
        <td>
          <a href="browse.cgi?id=[% row.product %]">[% row.product %]</a>
        </td>
        <td>
          <a href="show_bug.cgi?id=[% row.bug_id %]">[% row.bug_id %]</a>
        </td>
        <td>[% row.relevance FILTER format('%4g') %]</td>
        <td>[% row.status %]</td>
        <td>[% row.resolution %]</td>
        <td>[% row.summary %]</td>
      </tr>
      <tr>
        <td colspan=6>
          <blockquote><pre>[% row.comment FILTER html %]</pre></blockquote>
        </td>
      </tr>
    [% END %]
    </table>
  </p>

  <p>
    <b>The text you searched on was</b>
    <blockquote><pre>
    [% search_text FILTER html %]
    </pre></blockquote>
  </p>
[% END %]

[% PROCESS global/footer.html.tmpl %]
