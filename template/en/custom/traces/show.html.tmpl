[%# 1.0@bugzilla.org %]
[%# -*- mode: html -*- %]
[%# The contents of this file are subject to the Mozilla Public
  # License Version 1.1 (the "License"); you may not use this file
  # except in compliance with the License. You may obtain a copy of
  # the License at http://www.mozilla.org/MPL/
  #
  # Software distributed under the License is distributed on an "AS
  # IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
  # implied. See the License for the specific language governing
  # rights and limitations under the License.
  #
  # The Original Code is the Bugzilla Bug Tracking System.
  #
  # The Initial Developer of the Original Code is Olav Vitters
  # Corporation. Portions created by Netscape are
  # Copyright (C) 2005 Olav Vitters Corporation. All
  # Rights Reserved.
  #
  #%]

[%# INTERFACE:
  # action: action to take
  # hide_header: boolean; true if header and footer should not be shown.
  #%]

[% IF !hide_header %]
[% PROCESS global/variables.none.tmpl %]

[% IF action == "edit" %]
[% h1 = "Edit existing stack trace" %]
[% ELSIF action == "add" %]
[% h1 = "Add new stack trace" %]
[% END %]

[% PROCESS global/header.html.tmpl
    title = "Auto strack trace rejecting"
    h1 = h1 %]
[% END %]

[% USE GNOME %]

<form action="edittraces.cgi" method="POST">
<input type="hidden" name="action" value="[% action FILTER html %]">
[% IF action == "edit" && cur_trace.old_hash %]
<input type="hidden" name="old_hash" value="[% cur_trace.old_hash FILTER html %]">
[% END %]
[% IF cur_trace.exists('hash') %]<input name="hash" type="hidden" value="[% cur_trace.hash FILTER html %]">[% END %]
<style><!--
.foo tr { vertical-align: top }
 --></style>
<table border=0 cellpadding=4 cellspacing=0 class="foo">
<tr>
  <th align="right"><label for="dupe_of"><u>D</u>uplicate of?</label>:</th>
  <td><input name="dupe_of" id="dupe_of" accesskey="d" value="[% cur_trace.dupe_of FILTER html %]"></td>
</tr>
<tr>
  <th align="right"><label for="product_only"><u>P</u>roduct</label>:</th>
  <td><select name="product_id" id="product_id" accesskey="p">
    <option value="">Do not reject for a specific product</option>
    [% FOREACH product = products.keys.sort %]
      <option value="[% products.$product FILTER html %]" [% IF products.$product == cur_trace.product_id %] selected="selected"[% END %]>[% product FILTER html %]</option>
    [% END %]</select>
    [% IF cur_trace.dupe_of_bug && cur_trace.dupe_of_bug.bug_id
          && (!cur_trace.product_id || cur_trace.dupe_of_bug.product != cur_trace.product) %]
      <br><font color="red">[% terms.Bug %] [%+ cur_trace.dupe_of FILTER html %] product: [% cur_trace.dupe_of_bug.product FILTER html %]</font>
    [% END %]
  </td>
</tr>
<tr>
  <th align="right"><label for="version">Product version</label>:</th>
  <td>
    <input name="version" id="version" value="[% cur_trace.version FILTER html %]">
    <br>(leave empty to reject for all versions)
    [% IF cur_trace.dupe_of_bug && cur_trace.dupe_of_bug.bug_id
          && (!cur_trace.version || cur_trace.dupe_of_bug.version != cur_trace.version) %]
      <br><font color="red">[% terms.Bug %] [%+ cur_trace.dupe_of FILTER html %] version: [% cur_trace.dupe_of_bug.version FILTER html %]</font>
    [% END %]
  </td>
</tr>
<tr>
  <th align="right"><label for="gnome_version">GNOME version</label>:</th>
  <td><select name="gnome_version" id="gnome_version">
    <option value="">Do not reject using GNOME version (recommended)</option>
    [% FOREACH opt = legal_gnome_versions %]
      <option value="[% opt FILTER html %]" [% IF opt == cur_trace.gnome_version %] selected="selected"[% END %]>[% opt FILTER html %]</option>
    [% END %]</select>
    <br>(Bug-Buddy might not be able to detect the GNOME version)
    [% IF cur_trace.dupe_of_bug && cur_trace.dupe_of_bug.bug_id
          && (!cur_trace.gnome_version || cur_trace.dupe_of_bug.gnome_version != cur_trace.gnome_version) %]
      <br><font color="red">[% terms.Bug %] [%+ cur_trace.dupe_of FILTER html %] GNOME version: [% cur_trace.dupe_of_bug.gnome_version FILTER html %]</font>
    [% END %]
  </td>
</tr>
[% IF cur_trace.functions %]
  <tr>
    <th align="right"><label for="gnome_version">Detected functions</label>:</th>
    <td>
      [% cur_trace.functions FILTER html %]
      [% IF cur_trace.is_dupe %]
        <br><font color="red">ERROR: Stack trace is already rejected (perhaps for another product, etc). See <a href="edittraces.cgi?action=edit&amp;hash=[% cur_trace.hash FILTER url_quote %]" target="_new">here</a></font>
      [% END %]
    </td>
  </tr>
[% END %]
<tr>
  <th align="right"><label for="trace"><u>S</u>tack trace:</label></td>
  <td><textarea name="trace" id="trace" cols="80" rows="3" onFocus="this.rows=15" accesskey="s">[% cur_trace.trace FILTER html %]</textarea></td>
</tr>
<tr>
  <th align="right"><label for="reason"><u>R</u>eason:</label></td>
  <td><textarea name="reason" id="reason" cols="80" rows="3" onFocus="this.rows=15" accesskey="r">[% cur_trace.reason FILTER html %]</textarea></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td>
    [% IF action == "add" %]
      [% IF cur_trace.exists('hash') %]
        [% IF !cur_trace.functions %]<font color="red">ERROR: Could not parse any functions.</font><br>[% END %]
        <input type="submit" value="Re-parse above stack trace">
        [% IF cur_trace.functions && !cur_trace.is_dupe %] <input type="submit" name="do_add" value="Reject above stack trace">[% END %]
      [% ELSE %]
        <input type="submit" value="Parse stack trace for functions">
      [% END %]
    [% ELSIF action == "edit" %]
      <input type="submit" value="Validate changes">
      [% IF cur_trace.functions && !cur_trace.is_dupe %] <input type="submit" name="do_edit" value="Update above stack trace">[% END %]
    [% END %]
  </td>
</table>
</form>

[% PROCESS global/footer.html.tmpl IF !hide_header %]
