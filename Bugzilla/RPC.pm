
use strict;

use Bugzilla::RPC::createBug;
use Bugzilla::DB;

package Bugzilla::RPC;

use Bugzilla::DB;
use Bugzilla::Error;
use Bugzilla::Config;
use Bugzilla::Constants;
use Bugzilla::User;
use Bugzilla::Auth::Verify::DB;
use Bugzilla::GNOME;
use Bugzilla::Bug;
use Digest::MD5 qw(md5_hex);
use SortVersions;


# These are functions written by me or extracted from hard-to-extract places.
#  heavily hacked and custom function

# DO NOT USE THIS!
#
# WARNING FROM bugmaster@gnome.org
# 
# This XML-RPC experimental. I do NOT want to see other products using this
# interface. If I find out I will remove your product from bugzilla.gnome.org.
#


sub Add {
    my ($datain) = @_;
    my %data = %$datain;

    my $dbh = Bugzilla->dbh;
    my $template = Bugzilla->template;

    my $comment = $data{'comment'};

    my $given_gnome_version = exists $data{'gnome_version'} ? $data{'gnome_version'} : undef;
    if (!defined($given_gnome_version)) {
        my $cgi = Bugzilla->cgi;
        if ($cgi->user_agent() =~ /^Bug-Buddy: ([0-9]+\.[0-9]+[0-9.]+)$/) {
            $given_gnome_version = $1;
        }
    }
    if (defined($given_gnome_version) && (versioncmp($given_gnome_version, '2.24.0') == -1)) {
        Bugzilla::Error::ThrowUserError("please_try_with_newer_gnome")
    }


    # Check for oversized comments
    ValidateComment($comment);

    my @bug_fields = ("version", "rep_platform",
                      "bug_severity", "priority", "op_sys", "assigned_to",
                      "qa_contact",
                      "bug_status", "bug_file_loc", "short_desc",
                      "target_milestone", "status_whiteboard");


    my $reporter = $data{'reporter'};
    my $emailregexp = Param('emailregexp');
    my $createexp = Param('createemailregexp');

#    CheckEmailSyntax($reporter);
    my $user = Bugzilla::User->new_from_login($reporter);
    if ($user && $user->id) {
        # Account exists
    } elsif ($reporter !~ /$emailregexp/
             || $reporter =~ /[\\\(\)<>&,;:"\[\] \t\r\n]/
             || $reporter !~ /$createexp/ )
    {
        # Not a valid email address
        Bugzilla::Error::ThrowUserError("invalid_username",
                   { name => $reporter }, "abort");
    } else {
        # If we do not have a reporter at this point we need to create a new one
        # if possible. Means locking the tables.

        $dbh->bz_lock_tables('profiles WRITE', 'email_setting WRITE', 'tokens READ');

        if (!is_available_username($reporter)) {
            # Someone is trying to change their email address to the reporters email
            # address. Means we cannot create an account for that email address :-(

            # unlock
            $dbh->bz_unlock_tables();
            Bugzilla::Error::ThrowUserError("account_exists",
                     { email => $reporter });

        }

        # All verifications passed. Create the account.
        my $password = &::GenerateRandomPassword();
        insert_new_user($reporter, '', $password);

        # and unlock
        $dbh->bz_unlock_tables();

        my $message;
        $template->process('email/bug-buddy-account-created.txt.tmpl',
                           { password => $password,
                             account  => $reporter},
                           \$message);
        Bugzilla::BugMail::MessageToMTA($message);

        # get the newly created account
        $user = Bugzilla::User->new_from_login($reporter);
    }    

    my $userid = $user->id;

    if ($user->disabledtext) {
        Bugzilla::Error::ThrowUserError("account_disabled", 
                                        {'disabled_reason' => $user->disabledtext})
    }

    # If password was provided, verify it
    if (exists $data{'password'}) {
        # This is a very big hack as this assumes we are using the DB to authenticate against!
        my @result = Bugzilla::Auth::Verify::DB->authenticate($reporter, $data{'password'});

        if ($result[0] != AUTH_OK) {
            Bugzilla::Error::ThrowUserError("invalid_username_or_password");
        }
    }

    my $product = $data{'product'};

    # Force nautilus-cd-burner -> nautilus, see bug 352989
    if (lc($product) eq "nautilus-cd-burner") {
        $data{'product'} = 'nautilus';
        $product = 'nautilus';
        $data{'component'} = 'general';
    }

    my $product_id = get_product_id($product);
    if (!$product_id) {
        Bugzilla::Error::ThrowUserError("product_doesnt_exist", 
                                        { 'product' => $product })
    }

    my $component_id = get_component_id($product_id, $data{'component'});
    if (!$component_id) {
        Bugzilla::Error::ThrowUserError('component_not_valid',
                                        {'product' => $product, 
                                         'name' => $data{'component'}})
    }

    # Ensure correct case
    $product = $dbh->selectrow_array('SELECT name from products WHERE ' . 
                                     'id = ? ', undef, $product_id);
    my $component = get_component_name($component_id);

    $data{'short_desc'} = Bugzilla::Util::clean_text($data{'short_desc'});
    if (!exists $data{'short_desc'} || $data{'short_desc'} eq '') {
        Bugzilla::Error::ThrowUserError("require_summary"); }

    if (!defined $comment || !Bugzilla::Util::trim($comment)) { 
      Bugzilla::Error::ThrowUserError("description_required"); }

    # Force initialowner, initialqacontact
    ($data{'assigned_to'}, $data{'qa_contact'}) = $dbh->selectrow_array(
        'SELECT initialowner, initialqacontact FROM components ' .
        'WHERE id = ?', undef, $component_id);
    
    # Force bug_status
    $data{'bug_status'} = 'UNCONFIRMED';

    $data{'priority'} ||= Bugzilla::Config::Param('defaultpriority');
    Bugzilla::GNOME::check_field('priority', $data{'priority'});
    
    $data{'bug_severity'} ||= Bugzilla::Config::Param('defaultseverity');
    Bugzilla::GNOME::check_field('bug_severity', $data{'bug_severity'});

    if (Bugzilla::Config::Param("usetargetmilestone")) {
        $data{'target_milestone'} = $dbh->selectrow_array(
            'SELECT defaultmilestone FROM products WHERE id = ?', undef,
            $product_id);
    }

    $data{'op_sys'} = $::legal_opsys[0];
    $data{'rep_platform'} = $::legal_platform[0];

    # Bugzilla.gnome.org: Determine GNOME version from $version_details
    my $gnome_version = $::legal_gnome_version[0];
    my $match;
    my @gnome_version_matches;
    if ($given_gnome_version =~ /^([0-9]+\.[0-9]+)[. ]/) {
        # Extracted GNOME version from gien GNOME version
        my $match = $1;
        if (@gnome_version_matches = grep($_ =~ /(?:^|\/)\Q$match\E(?:\/|$)/,
                                          @::legal_gnome_version))
        {
            # Extracted GNOME version matched a value in @::legal_gnome_version, use it
            # The regexp assumed the GNOME version look like (example):
            #     2.1/2.2
            # or: 2.0
            $gnome_version = $gnome_version_matches[0];
        }
    }
    $data{'gnome_version'} = $gnome_version;
    push(@bug_fields, 'gnome_version');

    # Bugzilla.gnome.org: Set GNOME target
    $data{'gnome_target'} = $::legal_gnome_target[0];
    push(@bug_fields, 'gnome_target');

    $comment =~ s/\r\n/\n/g;     # Get rid of \r.
    $comment = Bugzilla::Util::trim($comment);

    my $err = "";
    my $version_set = 0;
    if (defined  ($::versions{$product} ))
    {
        my @version;
        my $version_x = $data{'version'};
        $version_x =~ s/^([\d\.]+\.)\d+$/$1x/;
    
        $version_set = 1;

        if (@version = grep(lc($_) eq lc($data{'version'}),
                            @{$::versions{$product}}))
 
        {
            $data{'version'} = $version[0];
        }
        elsif (defined  ($::versions{$product} ) &&
               ($data{'version'} =~ /^[\d\.]+\d+$/) &&
               (@version = grep /^$version_x$/i, @{$::versions{$product}}))
        {
            # We were able to match when the last number was replaced with a 'x'
            # e.g. '1.2.x' instead of '1.2.3'
            $err .= "Version: $data{'version'}\n";
            $data{'version'} = $version[0];
        }
        elsif (defined  ($::versions{$product} ) &&
               (@version = grep /^unspecified$/i, @{$::versions{$product}})
              )
        {
            $err .= "Version: $data{'version'}\n";
            $data{'version'} = $version[0];
        }
        else
        {
            $version_set = 0;
        }
    }

    if (!$version_set)
    {
        $data{'version'} = $::versions{$product}->[0];
        if ($data{'version'}) {
            $err .= "Version: $data{'version'}\n";
        }
    }

    my @used_fields;
    foreach my $field (@bug_fields) {
        if (exists $data{$field}) {
            push (@used_fields, $field);
        }
    }

    $data{'product_id'} = $product_id;
    push (@used_fields, 'product_id');
    $data{'component_id'} = $component_id;
    push (@used_fields, 'component_id');


    my @functions = get_traces_from_string($comment);
    if (@functions) {
        my $hash = md5_hex(join(" ", @functions));

        my ($dupe_id, $reason) = $dbh->selectrow_array(
            'SELECT dupe_of, reason
               FROM traces
              WHERE hash = ?
                AND (product_id = ? OR product_id IS NULL)
                AND (version = ? or version IS NULL)
                AND (gnome_version = ? or gnome_version IS NULL)',
            undef,
            ($hash, $product_id, $data{'version'}, $data{'gnome_version'}));

        if ($dupe_id) {
            # Log per stack trace how many have been rejected
            $dbh->do('UPDATE traces SET rejected = rejected + 1 WHERE hash = ?', 
                     undef, $hash);

            # XXX - check status of dupe
            #
            # if NEEDINFO.. foo 1
            # if OPEN, add stack?
            # if CLOSED, do nothing?
            if ($reason) {
                my $message;
                $template->process('email/bug-buddy-bugreport-rejected.txt.tmpl',
                                   {reason      => $reason,
                                    account     => $reporter,
                                    description => $comment,
                                    product     => $product,
                                    component_  => $component,
                                    dupe_of     => $dupe_id,
                                    hash        => $hash},
                                   \$message);
                Bugzilla::BugMail::MessageToMTA($message);
            }

            # Fake the reporter created the bug this strack is a duplicate of
            return $dupe_id;
        }
    }
    elsif ($data{'short_desc'} =~ /^crash in .*:/
           || bugreport_contains_gdb_trace($comment)) {
        my $reason = "Unfortunately, that stack trace is missing some elements that will help a lot
to solve the problem, so it will be hard for the developers to fix that crash.
Can you get us a stack trace with debugging symbols? Please see
http://live.gnome.org/GettingTraces for more information on how to do so.
Thanks in advance!";

        my $message;
        $template->process('email/bug-buddy-bugreport-rejected.txt.tmpl',
                           {reason      => $reason,
                            account     => $reporter,
                            description => $comment,
                            product     => $product,
                            component_  => $component},
                           \$message);
        Bugzilla::BugMail::MessageToMTA($message);
        

        # Bugreport that added this functionality
        return '393702';
    }

    # get current time
    my $timestamp = $dbh->selectrow_array("SELECT NOW()");
    my $sql_timestamp = $dbh->quote($timestamp);

    # Build up SQL string to add bug.
    my $sql = "INSERT INTO bugs " .
      "(" . join(",", @used_fields) . ", reporter, delta_ts, " .
      "estimated_time, remaining_time) " .
      "VALUES (";
    
    foreach my $field (@used_fields) {
        $sql .= $dbh->quote($data{$field}) . ",";
    }




    # Remove repeating '(no debugging symbols found)'
    $comment =~ s/(?:\(no debugging symbols found\)\r?\n)+/(no debugging symbols found)\n/g;
    # If comment is all whitespace, it'll be null at this point. That's
    # OK except for the fact that it causes e-mail to be suppressed.
    $comment = $comment ? $comment : " ";
    if ($err) {
        $comment = $err . "\n" . $comment;
    }
    
    $sql .= "$userid, $sql_timestamp, ";
    
    # Time Tracking
    $sql .= "0, 0";
    #}
    $sql .= ")";

    # Add the bug report to the DB.
    $dbh->bz_lock_tables('bugs WRITE', 'bug_group_map WRITE', 'longdescs WRITE',
                         'cc WRITE', 'keywords WRITE', 'dependencies WRITE',
                         'bugs_activity WRITE', 'groups READ', 'user_group_map READ',
                         'keyworddefs READ', 'fielddefs READ');
                                                                   

    Bugzilla::DB::PushGlobalSQLState();
    Bugzilla::DB::SendSQL($sql);

    # Get the bug ID back.
    my $bugid = $dbh->bz_last_key('bugs', 'bug_id');
    Bugzilla::DB::PopGlobalSQLState();

    # Add the comment
    Bugzilla::DB::SendSQL("INSERT INTO longdescs (bug_id, who, bug_when, thetext)
             VALUES ($bugid, $userid, $sql_timestamp, " . Bugzilla::DB::SqlQuote($comment) . ")");

   
    $dbh->do("UPDATE bugs SET creation_ts = ? WHERE bug_id = ?",
              undef, ($timestamp, $bugid));

    $dbh->bz_unlock_tables();

    Bugzilla::BugMail::Send($bugid, { 'changer' => $userid });

    return $bugid;
}


# Bugzilla needs some serious refactoring love.  These are functions
# from global.pm that should be moved into a package that doesn't mess
# around with CGI environment


sub get_product_id {
    my ($product) = @_;
    my $dbh = Bugzilla->dbh;

    my $product_id = $dbh->selectrow_array('SELECT id from products WHERE ' .
                                           'name = ? ', undef, $product);
                                             
    return $product_id;
}

sub get_component_id {
    my ($prod_id, $comp) = @_;
    my $dbh = Bugzilla->dbh;
    return undef unless ($prod_id && ($prod_id =~ /^\d+$/));

    my ($comp_id) = $dbh->selectrow_array('SELECT id FROM components ' .
                                          'WHERE product_id = ? AND name = ?',
                                          undef, ($prod_id, $comp));

    return $comp_id;
}

sub get_component_name {
    my ($comp_id) = @_;
    my $dbh = Bugzilla->dbh;
    die "non-numeric comp_id '$comp_id' passed to get_component_name"
      unless ($comp_id =~ /^\d+$/);

    my ($comp) = $dbh->selectrow_array('SELECT name FROM components ' .
                                       'WHERE id = ?', undef, $comp_id);

    return $comp;
}


1;
