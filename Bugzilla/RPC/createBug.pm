
use strict;

our ($userid);

package Bugzilla::RPC;

# Public RPC Function
#
# createBug(data)
# 
# Returns: [id]

sub createBug {
  shift if UNIVERSAL::isa($_[0] => __PACKAGE__);

  my ($data) = @_;

  my $result = Bugzilla::RPC::Add(\%$data);

  return $result;

}

1;


