# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Olav Vitters <olav@bkor.dhs.org>

use strict;

package Bugzilla::GNOME;

use Bugzilla;
use Bugzilla::Config;
use Bugzilla::Error;
use Bugzilla::Util;
use base qw(Exporter);
@Bugzilla::GNOME::EXPORT = qw(
    get_traces_from_string
    bugreport_contains_gdb_trace
); 

=head1 get_traces_from_string

takes a string that may contain a stack trace. If it does contain
a viable stack trace, the first five useful functions are returned.

=cut
sub get_traces_from_string {
    my ($string) = @_;
    my @functions;

    @functions = get_gdb_traces_from_string($string);
    @functions = get_python_traces_from_string($string) unless (@functions);

    return @functions;
}

sub get_gdb_traces_from_string {
  my ($string) = @_;
  my @functions = ();
  my $function_index = -1;
  my @lines = split('\n', $string);

  # For a simplification, all of this could be done in one
  # regexp. [Ben Frantzdale] You volunteering to write it, Ben? :)
  # [Luis 'not great with regexp' Villa]

  my $possible_starts = join('|', ('<signal handler called>',
                                   'killpg',
                                   'sigaction',
                                   'sigsuspend',
                                   '\(gdb\) bt',
                                   'gnome_init',
                                   'g_log'));
  my $bad_functions = join('|', ('__kernel_vsyscall',
                                 'raise',
                                 'abort'));

  foreach my $line (@lines) {
  # Here, we try to ignore things before <signal handler> and/or killpg()
    if ( $line =~ /$possible_starts/ ) {
      # Now we've started looking for real functions, or reset after
      # reading g_log tops
      $function_index = 0;
    }
    elsif ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i &&
            $function_index >= 0){
      # eliminate dups
      if ($function_index != 0 && $functions[$function_index-1] eq $1) {
        next;
      }

      # eliminate meaningless functions in the stack trace
      if ($1 =~ /$bad_functions/) {
        next;
      }
      $functions[$function_index]=$1;
      $function_index++;
    }
    # We stop after five functions are found:
    last if $function_index > 4;
  }

  #did we go all the way through without getting any frames?
  #if so, we've got a weird trace; we'll pick it up ourselves [since we take for granted there is actually a trace there]
  if ( $function_index == -1 ) {
      foreach my $line (@lines) {
          if ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i ){
              push(@functions, $1);
              $function_index++;
          }
          # We stop after five functions are found:
          last if $function_index > 3; # only go to three because we started at -1
      }
  }

  return grep {$_} @functions;
}

sub get_python_traces_from_string {
  my ($string) = @_;
  my @functions = ();
  my $function_index = -1;
  my @lines = split('\n', $string);

  # For a simplification, all of this could be done in one
  # regexp. [Ben Frantzdale] You volunteering to write it, Ben? :)
  # [Luis 'not great with regexp' Villa]

  my $possible_starts = join('|', ('<signal handler called>',
                                   'killpg',
                                   'sigaction',
                                   'sigsuspend',
                                   '\(gdb\) bt',
                                   'gnome_init',
                                   'g_log'));


  my $in_stacktrace = 0;
  foreach my $line (@lines) {
  # Here, we try to ignore things before <signal handler> and/or killpg()
    if ( $line =~ /^Traceback \(most recent call last\):/ ) {
      # Now we've started looking for real functions, or reset after
      # reading g_log tops
      $in_stacktrace = 1;
      next;
    }
    elsif ( $in_stacktrace && $line =~ /^  File "[^"]+", line [0-9]+, in (<?(?:\w|::|_)+>?)$/i ) {
        push (@functions, $1);
    }
    elsif ( $in_stacktrace && ($line =~ /^(EOFError)$/ || $line =~ /^([A-Z][A-Za-z]+): /) ) {
        push (@functions, $1);
        @functions = reverse(@functions);
        return splice(@functions, 0, 5);
    }
    elsif ( $in_stacktrace && $line =~ /^[^ ]/ ) {
        $in_stacktrace = 0;
    }
  }

  return ();
}

sub get_traces_info {
    my $self = shift;

    my ($hash) = @_;
    my $dbh = Bugzilla->dbh;

    my @args = ();
    push @args, $hash if $hash;

    return $dbh->selectall_arrayref(
        'SELECT hash, dupe_of, product_id, user_id, version, gnome_version, 
                trace, products.name AS product, rejected, reason
           FROM traces
      LEFT JOIN products
             ON traces.product_id = products.id' . (defined $hash ? '
          WHERE hash = ?' : '') . '
       ORDER BY dupe_of',  {'Slice' => {}}, @args);

}

sub check_field {
    my ($name, $value, $legalsRef, $no_warn) = @_;
    my $dbh = Bugzilla->dbh;

    # If $legalsRef is undefined, we use the default valid values.
    unless (defined $legalsRef) {
        $legalsRef = get_legal_field_values2($name);
    }

    if (!defined($value)
        || trim($value) eq ""
        || lsearch($legalsRef, $value) < 0)
    {
        return 0 if $no_warn; # We don't want an error to be thrown; return.
        trick_taint($name);

        my ($result) = $dbh->selectrow_array("SELECT description FROM fielddefs
                                              WHERE name = ?", undef, $name);

        my $field_desc = $result ? $result : $name;
        ThrowCodeError('illegal_field', { field => $field_desc });
    }
    return 1;
}


sub get_legal_field_values2 {
    my ($field) = @_;
    my $dbh = Bugzilla->dbh;
    my $result_ref = $dbh->selectcol_arrayref(
         "SELECT value FROM $field
           WHERE isactive = ?
        ORDER BY sortkey, value", undef, (1));
    return $result_ref;
}


sub bugreport_contains_gdb_trace {
    my ($comment) = @_;

    return $comment =~ /Backtrace was generated from/m;
}

1;
