#!/usr/bin/perl -w

# Automatically bans people from Bugzilla

my $htadir = $ENV{DOCUMENT_ROOT};
my $htafile = "/\.htaccess";

# Form full pathname to .htaccess file
my $htapath = "$htadir"."$htafile";

# Get the bad-bot's IP address, convert to regular-expressions (regex) format by escaping all
# periods.
my $remaddr = $ENV{REMOTE_ADDR};

# Get User-agent and current time
my $usragnt = $ENV{HTTP_USER_AGENT};
$usragnt =~ s/[\x00-\x1F\x7F]+/ /g; # remove control characters
my $date = scalar localtime(time);

# Open the .htaccess file and wait for an exclusive lock. This prevents multiple instances of this
# script from running past the flock statement, and prevents them from trying to read and write the
# file at the same time, which would corrupt it. When .htaccess is closed, the lock is released.
#
# Open existing .htaccess file in r/w append mode, lock it, rewind to start, read current contents
# into array.
###open(HTACCESS,"+>>$htapath") || die $!;
###flock(HTACCESS,2);
#seek(HTACCESS,0,0);
#@contents = <HTACCESS>;
# Empty existing .htaccess file, then write new IP ban line and previous contents to it
#truncate(HTACCESS,0);
###print HTACCESS ("\n# blocked by rfc.cgi - $date - $usragnt\ndeny from $remaddr\n");
#print HTACCESS (@contents);
# close the .htaccess file, releasing lock - allow other instances of this script to proceed.
###close(HTACCESS);

# Write html output to server response
print <<_EOF_;
Content-type: text/html

<html><head><title>Invalid URL</title></head>

<body text=\"#000000\" bgcolor="#FFFFFF">
<p>The link you followed is often used by spiders and spam harvesters. Please email bugmaster\@gnome.org the URL you followed and we'll tell you how to avoid this warning.</p>
<p>Apologies for the inconvience<br>
The GNOME bugmasters</p></body>
</html>
_EOF_

exit;

