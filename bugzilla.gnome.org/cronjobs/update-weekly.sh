#!/bin/bash
#
# This script is cron'ed to run every Saturday.

REPORTS_DIR=../../reports

# Send the Gnome weekly summary report.
(cd $REPORTS_DIR && ./email_gnome_summary_report.pl )
