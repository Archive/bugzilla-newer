#!/usr/bin/perl -w
#
# create-mostfreq-xml.cgi
#
# Written for ximian.bugzilla.org by Luis Villa (louie@ximian.com).
#
# Adapted to gnome.bugzilla.org by Wayne Schuller (k_wayne@linuxpower.org)
#
# Heavily modified to use recent-mostfrequent.cgi output by Elijah
# Newren (newren@gmail.com.
#
# Summary: Output an xml file of most frequently and recently reported
# bugs for bug-buddy.  This allows bug-buddy users to see if a bug has
# already been reported.
#
# The output from this script is not designed for a web browser. It is plain
# xml pumped to STDOUT.
#
#TODO: 
#	- doesn't limit number of bugs per component yet
#	- Use the shadow database?
#	- write a proper DTD

use XML::Generator;
use strict;


BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /cronjobs$/) {
        chdir "../..";
    }
}

use lib qw(.);

push @INC, "reports/."; # many scripts now are in the reports subdirectory

use Bugzilla;

my $dbh = Bugzilla->dbh;

require "globals.pl";

require "recent-mostfrequent-utils.pl";

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

my ($days, $print_warnings) = (30, 0, 0);
my $num_bugs_to_show = 20;

# Get the duplicate data
my $dupe_data = get_recent_mostfrequent_table($days, $print_warnings);
my @dupe_data = @$dupe_data;

# Get the top duplicates, in order from most duplicated to least
my @sorted_dupe_data = reverse sort {$a->[1] <=> $b->[1]} @dupe_data;
my @toplist = splice(@sorted_dupe_data, 0, $num_bugs_to_show);

# We will store the xml to print in @products
my @products = ();

my $query = '
        SELECT products.name AS product, components.name AS component, short_desc
          FROM bugs
    INNER JOIN products
            ON bugs.product_id = products.id
    INNER JOIN components
            ON bugs.component_id = components.id
         WHERE bugs.bug_id = ?';

my $sth = $dbh->prepare($query);

# For each common duplicate (reverse the list because bug-buddy is weird)...
foreach my $listref (reverse @toplist) {
    # Get the product, component, and short_desc for this bug
    $sth->execute($listref->[0]);
    my ($product, $component, $short_desc) = $sth->fetchrow_array;
    my ($bug_id, $dupes) = ($listref->[0], $listref->[1]);

    push @products, 
        $xml->product({name => $product},
            $xml->component({name => $component},
                $xml->bug({bugid => $listref->[0]},
                $xml->desc($short_desc),
                $xml->url('http://bugzilla.gnome.org/show_bug.cgi?id='.$bug_id),
                $xml->dups($dupes)
            )
        ));
}

my $total = $xml->products(@products);
print qq[<!DOCTYPE products SYSTEM "http://bugzilla.gnome.org/bugzilla.dtd">\n];
print $total . "\n";
