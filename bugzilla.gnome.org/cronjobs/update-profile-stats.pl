#!/usr/bin/perl -wT

use strict;

BEGIN {
  use Cwd;
  my $dir = cwd;
  if ($dir && $dir =~ /cronjobs$/) {
    chdir "../..";
  }
}
use lib qw(.);
require "globals.pl";
use Bugzilla;

# Get the previous time and only update stats for users who made a
# comment, closed a bug, or reported a bug since that time
my $prev_time;
if (open(BLABLABLA, "<bugzilla.gnome.org/cronjobs/lasttime.txt")) {
  $prev_time = <BLABLABLA>;
  close(BLABLABLA);
}
if (!$prev_time) {
  $prev_time = "1900-01-01 01:01:01";
}

# Get the current time
my $dbh = Bugzilla->dbh;
my $time = $dbh->selectrow_array("SELECT NOW()");

# Sanity check; make sure $time is newer
trick_taint($prev_time);
my $quoted_prev_time = $dbh->quote($prev_time);
my $is_newer =
  $dbh->selectrow_array("SELECT '$time' > $quoted_prev_time");
die "Timestamp illogical" if !$is_newer;

print "Updating all userids that have done anything since $prev_time\n";

# Get all the relevant ids
print "Getting userids of those who have added comments\n";
my $comment_ids =
  $dbh->selectcol_arrayref("SELECT DISTINCT who
                              FROM longdescs
                             WHERE bug_when > $quoted_prev_time");
print "Getting userids of those who have closed bugs\n";
my $status_field_id = 
  $dbh->selectrow_array("SELECT fieldid FROM fielddefs
                         WHERE name = 'bug_status'");
my $closed_ids =
  $dbh->selectcol_arrayref("SELECT DISTINCT who
                              FROM bugs_activity, bugs
                             WHERE bugs_activity.bug_id = bugs.bug_id
                               AND bugs_activity.fieldid = $status_field_id
                               AND bugs.bug_status IN ('RESOLVED','CLOSED',
                                                       'VERIFIED')
                               AND bugs_activity.added IN ('RESOLVED','CLOSED')
                               AND delta_ts > $quoted_prev_time");
print "Getting userids of those who have reported bugs\n";
my $reported_ids =
  $dbh->selectcol_arrayref("SELECT DISTINCT reporter
                              FROM bugs
                             WHERE bugs.creation_ts > $quoted_prev_time");
my @all_ids = (@{$comment_ids}, @{$closed_ids}, @{$reported_ids});
my %unique_hash = map { $_ => 1 } @all_ids;
my @update_ids = keys %unique_hash;

# Prepare a bunch of queries for each user we need to update
my $sth_get_comment_count =
  $dbh->prepare("SELECT COUNT(thetext) FROM longdescs WHERE who = ?");
my $sth_get_bugs_closed = 
  $dbh->prepare("SELECT COUNT(bugs.bug_id)
                  FROM bugs
                 INNER JOIN bugs_activity ON bugs.bug_id = bugs_activity.bug_id
                 WHERE bugs.bug_status IN ('RESOLVED','CLOSED','VERIFIED')
                   AND bugs_activity.added IN ('RESOLVED','CLOSED')
                   AND bugs_activity.bug_when =
                         (SELECT MAX(bug_when)
                            FROM bugs_activity ba
                           WHERE ba.added IN ('RESOLVED','CLOSED')
                             AND ba.removed IN ('UNCONFIRMED','REOPENED',
                                                'NEW','ASSIGNED','NEEDINFO')
                             AND ba.bug_id = bugs_activity.bug_id)
                   AND bugs_activity.who = ?");
my $sth_get_bugs_reported =
  $dbh->prepare("SELECT COUNT(DISTINCT bug_id)
                 FROM bugs
                 WHERE bugs.reporter = ?
                   AND NOT (bugs.bug_status = 'RESOLVED' AND 
                            bugs.resolution IN ('DUPLICATE','INVALID','NOTABUG',
                                                'NOTGNOME','INCOMPLETE'))");

# Run the queries for each userid we need to update
print "Finding the stats for the " . scalar(@update_ids) . " users...\n";
my $user_num = 0;
my %user_points;
foreach my $userid (@update_ids) {
  $sth_get_comment_count->execute($userid);
  my $comment_count = $sth_get_comment_count->fetchrow_array;

  $sth_get_bugs_closed->execute($userid);
  my $bugs_closed = $sth_get_bugs_closed->fetchrow_array;

  $sth_get_bugs_reported->execute($userid);
  my $bugs_reported = $sth_get_bugs_reported->fetchrow_array;

  my $points =
    log(1 + $comment_count) / log(10) + 
    log(1 + $bugs_closed)   / log(2) + 
    log(1 + $bugs_reported) / log(2);
  my $rounded_points = int($points + 0.5);

  # Special case users:
  #      37 - 'unknown@gnome.bugs'
  #     138 - 'debbugs-export@gnome.bugs'
  #   53166 - 'bugbuddy-import@ximian.com'
  if ($userid == 37  ||
      $userid == 138 ||
      $userid == 53166) {
    $rounded_points = 0;
  }

  $user_points{$userid} = $rounded_points;

  if (++$user_num % 1000 == 0) {
    print "  ...finished user $user_num\n";
  }
}

# Save all the new results
print "Now saving all the stats...\n";
my $sth_update_points =
  $dbh->prepare("UPDATE profiles SET points = ? WHERE userid = ?");
$dbh->bz_lock_tables('profiles WRITE');
foreach my $userid (@update_ids) {
  $sth_update_points->execute($user_points{$userid}, $userid);
}
$dbh->bz_unlock_tables();

# Save the time used
open(BLABLABLA, ">bugzilla.gnome.org/cronjobs/lasttime.txt") || die "Crap!\n";
print BLABLABLA $time;

print "profiles.points successfully updated up to $time.\n"
