#!/usr/bin/perl -wT

use XML::Generator;
use strict;
use vars qw(
    @legal_severity
    @legal_priority
    @legal_platform
    @legal_opsys
    @legal_bug_status
    @legal_resolution
);

BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /cronjobs$/) {
        chdir "../..";
    }
}

use lib qw(.);

require "globals.pl";

use Bugzilla;

GetVersionTable();

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

my $total = '';
my @total_xml;

{
    my @fields;
    foreach (@legal_severity) {
	push @fields, $xml->severity($_);
    }

    push @total_xml, $xml->severities(@fields);
}

{
    my @fields;
    foreach (@legal_priority) {
	push @fields, $xml->priority($_);
    }

    push @total_xml, $xml->priorities(@fields);
}

{
    my @fields;
    foreach (@legal_opsys) {
	push @fields, $xml->opsys($_);
    }

    push @total_xml, $xml->opsys_list(@fields);
}

{
    my @fields;
    foreach (@legal_platform) {
	push @fields, $xml->platform($_);
    }

    push @total_xml, $xml->platforms(@fields);
}

{
    my @fields;
    foreach (@legal_bug_status) {
	push @fields, $xml->bug_state($_);
    }

    push @total_xml, $xml->bug_states(@fields);
}

{
    my @fields;
    foreach (@legal_resolution) {
	push @fields, $xml->resolution($_);
    }

    push @total_xml, $xml->resolutions(@fields);
}

$total = $xml->localconfig (@total_xml);

print qq[<!DOCTYPE localconfig SYSTEM "http://bugzilla-test.gnome.org/bugzilla.dtd">\n];
print $total . "\n";



