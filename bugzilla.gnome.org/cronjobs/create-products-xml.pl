#!/usr/bin/perl -wT

use DBI;
use XML::Generator;
use strict;

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /cronjobs$/) {
        chdir "../..";
    }
}

use lib qw(.);

require "globals.pl";

use Bugzilla;


my $dbh = Bugzilla->dbh;

my $sth = $dbh->prepare("SELECT id,name,description,milestoneurl,disallownew,defaultmilestone FROM products");
my $rv = $sth->execute;

my @products;

my $sth_comp = $dbh->prepare('SELECT name AS value, description FROM components WHERE product_id = ?');
my $sth_vers = $dbh->prepare('SELECT value FROM versions WHERE product_id = ?');

my $row;
while ($row = $sth->fetchrow_hashref) {
    my $product = $row->{name};
    my $product_id = $row->{id};

    # (by fherrera) l10n product is breaking this xml generation.
    # This is a temporal workaround
    next if $product eq 'l10n';

    # (by ovitters) gnome-backgrounds contain some background images.
    # It gets a lot of misfiled bugreports. As b.g.o and bug-buddy do not yet
    # allow attachments, it isn't useful
    next if $product eq 'gnome-backgrounds';

    $sth_comp->execute($product_id);

    my $component;
    my @component_xml;
    while ($component = $sth_comp->fetchrow_hashref) {
	push @component_xml, $xml->component({value		=> $component->{value},
					      description	=> $component->{description}});
    }

    $sth_vers->execute($product_id);

    my $version;
    my @version_xml;
    while ($version = $sth_vers->fetchrow_hashref) {
	push @version_xml, $xml->version({value => $version->{value}});
    }

    my @fields;
    push @fields, 'milestoneurl' => $row->{milestoneurl} if $row->{milestoneurl} ne '';
    push @fields, 'defaultmilestone' => $row->{defaultmilestone} if $row->{defaultmilestone} ne '---';

    push @products, $xml->product({name			=> $row->{name},
				   description		=> $row->{description},
				   disallownew		=> $row->{disallownew},
				   @fields},
				  @component_xml,
				  @version_xml
				  );
}

my $total = $xml->products(@products);

print qq[<!DOCTYPE products SYSTEM "http://bugzilla-test.gnome.org/bugzilla.dtd">\n];
print $total . "\n";



