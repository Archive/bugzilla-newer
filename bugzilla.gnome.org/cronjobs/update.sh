#!/bin/bash
#
# This script is cron'ed to run every 30 minutes.

OUTPUT_DIR="`pwd`"
BUG_BUDDY_DIR=..
BUGZILLA_DIR=$OUTPUT_DIR/../..
REPORTS_DIR=$OUTPUT_DIR/../../reports

# Needed for running the cgi scripts
REQUEST_METHOD="GET"
export REQUEST_METHOD

#
# Bug buddy stuff
#
(./create-products-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-products.xml)
(xmllint --noout $BUG_BUDDY_DIR/bugzilla-products.xml 2>/dev/null; if [ $? = 1 ]; then xmllint --recover  $BUG_BUDDY_DIR/bugzilla-products.xml > temp.xml 2>/dev/null && mv -f temp.xml $BUG_BUDDY_DIR/bugzilla-products.xml; fi)
(./create-config-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-config.xml)
(./create-mostfreq-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-mostfreq.xml)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-products.xml >  md5sums)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-config.xml   >> md5sums)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-mostfreq.xml >> md5sums)

#
# Update stats in user profiles
#
./update-profile-stats.pl 2>&1 > /dev/null

#
# Update cached reports
#
(cd $BUGZILLA_DIR

## FIXME!  Do this in perl and use
##   SELECT value FROM gnome_version ORDER BY sortkey DESC LIMIT 3;
## We would still need to manually fix up page.cgi?id=reports.html, but at
## least it'd be one less thing requiring manual fixup
# 2.22.x report
QUERY_STRING="version=2.27/2.28" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.28.html
if  test -f $OUTPUT_DIR/temp2.28.html
then mv $OUTPUT_DIR/temp2.28.html $REPORTS_DIR/gnome-228-report.html
fi
# 2.20.x report
QUERY_STRING="version=2.25/2.26" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.26.html
if  test -f $OUTPUT_DIR/temp2.26.html
then mv $OUTPUT_DIR/temp2.26.html $REPORTS_DIR/gnome-226-report.html
fi
# 2.18.x report
QUERY_STRING="version=2.23/2.24" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.24.html
if  test -f $OUTPUT_DIR/temp2.24.html
then mv $OUTPUT_DIR/temp2.24.html $REPORTS_DIR/gnome-224-report.html
fi



