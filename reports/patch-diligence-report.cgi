#!/usr/bin/perl -wT
#
# Copyright 2004-2005, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use Bugzilla; 
require "CGI.pl";

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $template = Bugzilla->template;
my $vars = {};

require "globals.pl";

my $filename = "patch-diligence-report.html";
my $format = GetFormat("reports/patch-diligence",
                       scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));
print $cgi->header($format->{'ctype'});

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
Bugzilla->login();

# make sensible defaults.
my $min_days = 7;      # Don't show patches younger than this
my $max_days = 180;    # Don't show patches older than this
my $cutoff = 4;        # Don't list products with fewer patches than this
my $use_blocked = 0;   # (boolean) whether BLOCKED_BY_FREEZE counts as review
my $count_obsolete =0; # (boolean) whether to include obsolete patches in count
my $only_core_gnome=0; # (boolean) whether to only show core gnome products

if (defined $cgi->param('min_days') && $cgi->param('min_days') ne ""){
  $min_days = $cgi->param('min_days');
  detaint_natural($min_days) || die "min_days parameter must be a number";
}

if (defined $cgi->param('max_days') && $cgi->param('max_days') ne ""){
  $max_days = $cgi->param('max_days');
  detaint_natural($max_days) || die "max_days parameter must be a number";
}

if (defined $cgi->param('cutoff') && $cgi->param('cutoff') ne ""){
  $cutoff = $cgi->param('cutoff');
  detaint_natural($cutoff) || die "cutoff parameter must be a number";
}

if (defined $cgi->param('use_blocked') && $cgi->param('use_blocked') ne ""){
  $use_blocked = $cgi->param('use_blocked');
  detaint_natural($use_blocked) || die "use_blocked parameter must be 0 or 1";
}

if (defined $cgi->param('count_obsolete') &&
    $cgi->param('count_obsolete') ne ""){
  $count_obsolete = $cgi->param('count_obsolete');
  detaint_natural($count_obsolete) || die "count_obsolete must be 0 or 1";
}

if (defined $cgi->param('only_core_gnome') &&
    $cgi->param('only_core_gnome') ne ""){
  $only_core_gnome = $cgi->param('only_core_gnome');
  detaint_natural($only_core_gnome) || die "only_core_gnome must be 0 or 1";
}

  #
  # Step 1: Initialize a whole bunch of fields that will determine
  # modifications to the query
  #
  my $status_none = GetStatusID('none');

  my $include_obsolete = "";
  my $include_obsolete_where = "attachments.isobsolete = '0' AND";
  if ($count_obsolete) {
    $include_obsolete = "attachments.isobsolete='0' and";
    $include_obsolete_where = ""
  }

  my $include_blocked = "and bugs.keywords ne 'BLOCKED_BY_FREEZE'";
  $include_blocked = "" unless $use_blocked;

  my $use_prod_table = "";
  my $is_gnome = "";
  if ($only_core_gnome) {
    $use_prod_table = "LEFT JOIN products ON bugs.product_id = products.id";
    $is_gnome = "AND products.classification_id IN (" .
                     join(",",map { $dbh->quote(get_classification_id($_)) } 
                                    GnomeClassifications() ) . ")";
  }

  #
  # Step 2: Create and run the query
  #
  my $query = "
         SELECT bugs.bug_id, bugs.product_id, attachments.attach_id, 
                (attachments.status_id = $status_none AND $include_obsolete
                 (bugs.bug_status='UNCONFIRMED' OR bugs.bug_status='NEW'
                  OR bugs.bug_status='ASSIGNED' OR bugs.bug_status='REOPENED')
                 $include_blocked) AS is_unreviewed
           FROM attachments
      LEFT JOIN bugs
             ON attachments.bug_id = bugs.bug_id
                $use_prod_table
          WHERE attachments.ispatch='1'
            AND $include_obsolete_where
                attachments.creation_ts >= NOW() - " .
                    $dbh->sql_interval($max_days, 'DAY') . "
            AND attachments.creation_ts <= NOW() - " .
                    $dbh->sql_interval($min_days, 'DAY') . "
                $is_gnome
       ORDER BY bugs.product_id, attachments.bug_id";

  my $raw_data = $dbh->selectall_arrayref($query);

  #
  # Step 3: Record all the data, coalescing data from the same product
  #
  my %producthash = ();
  foreach my $rowRef (@$raw_data) {
    my ($bug_id, $product_id, $attach_id, $unreviewed) = @$rowRef;
    my $hashref = $producthash{$product_id};

    if (!defined $hashref) {
      $producthash{$product_id} = {
        name             => get_product_name($product_id),
        percentage       => 0,
        unreviewed_count => 0,
        patch_count      => 0,
        unreviewed_list  => "buglist.cgi?bug_id=",
        patch_list       => "buglist.cgi?bug_id="
        };
      $hashref = $producthash{$product_id};
    }

    if ($unreviewed) {
      $hashref->{unreviewed_list} .= "$bug_id,";
      $hashref->{unreviewed_count}++;
    }
    $hashref->{patch_list} .= "$bug_id,";
    $hashref->{patch_count}++;
  }

  #
  # Step 4: Gather statistics on the coalesced data and throw out the ones
  # that we don't want.
  #
  my @productlist = ();
  my ($total_unreviewed_count, $total_count) = (0, 0);

  while (my ($product_id, $hashref) =  each(%producthash)) {
    if ($hashref->{patch_count} >= $cutoff) {
      my $percentage = $hashref->{unreviewed_count}/$hashref->{patch_count};
      $hashref->{percentage} = sprintf("%3.0f", 100 * $percentage);

      $total_unreviewed_count += $hashref->{unreviewed_count};
      $total_count            += $hashref->{patch_count};

      push @productlist, $hashref;
    }
  }

  my $overall_percentage = 
    sprintf("%3.0f", 100 * $total_unreviewed_count / $total_count);

  #
  # Step 5: Pass the data on to the template
  #

  # CGI params / defaults:
  $vars->{'only_core_gnome'} = $only_core_gnome;
  $vars->{'min_days'} = $min_days;
  $vars->{'max_days'} = $max_days;
  $vars->{'cutoff'} = $cutoff;
  $vars->{'count_obsolete'} = $count_obsolete;
  $vars->{'use_blocked'} = $use_blocked;

  # Retrieved from queries:
  @productlist = sort {$a->{percentage} <=> $b->{percentage} ||
                       $a->{name} cmp $b->{name}} @productlist;
  $vars->{'productlist'} = \@productlist;
  $vars->{'total_unreviewed_count'} = $total_unreviewed_count;
  $vars->{'total_count'} = $total_count;
  $vars->{'overall_percentage'} = $overall_percentage;

  $template->process("$format->{'template'}", $vars)
    || ThrowTemplateError($template->error());
