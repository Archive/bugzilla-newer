#!/usr/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express OR
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.

# This is a cgi to reports bugs filed against 'core' desktop modules
# in the previous N days.

# NOTES: 
# * this was last updated for the 2.5 module list on 12/31/2003
# * pass 'days=X' to see X days before today.
# * pass 'includeowen=1' to see glib/gtk/pango, since owen has asked
#   volunteers not to touch those; must also includelibs for this to
#   work
# * pass 'includelibs=1' to see libs, since libs are difficult for
#   most volunteers to triage; doesn't include glib/gtk/pango without
#   an explicit 'includeowen' (though, maybe we should remove
#   includeowen now since only desktop stuff is on by default?)
# * pass 'ignoretriaged=1' to ignore bugs with the bugsquad keyword.
# * pass 'keywords=FOO' to show only bugs with FOO as a keyword.

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use vars qw($vars);

require "globals.pl";

use Bugzilla;

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
 
require "globals.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

#save ourselves some writing later
my $days = $cgi->param('days');
$days ||= 1; #default to 'today'
detaint_natural($days) || die "days parameter must be a number";

#build the query
#structure of the query is such:
#first list all acceptable states
#then dates [FIXME]
#then list of all top-level modules- if the module is a top-level 
#  bugzilla product, add things here
#then list of all lower-level modules- for example, we list applets
#  separately here because we wish to ignore many things in the 
#  gnome-applets product in bugzilla.


my $query = 
       "SELECT bugs.bug_id
          FROM bugs
    INNER JOIN products
            ON bugs.product_id = products.id
    INNER JOIN classifications
            ON products.classification_id = classifications.id
         WHERE bugs.creation_ts >= DATE_SUB(NOW(), INTERVAL $days DAY)
           AND bugs.creation_ts <= now()
           AND
    ( ";
if(!$cgi->param('ignoretriaged')){
        $query.=" bugs.bug_status = 'UNCONFIRMED' OR ";
}
$query .= "
     bugs.bug_status = 'NEW' OR 
     bugs.bug_status = 'ASSIGNED' OR 
     bugs.bug_status = 'REOPENED'
     )
AND 
    (
    classifications.name = 'desktop'";

if($cgi->param('includelibs')){
    $query .= "
    OR
      classifications.name = 'platform' OR
      classifications.name = 'bindings'";
}
    $query.=')';

# re-add gtk/pango/etc. if necessary for some reason
unless ($cgi->param('includeowen')){
    $query.= "
    AND NOT
        (
         products.name = 'gtk+' OR
         products.name = 'gtk-docs' OR
         products.name = 'pango' OR
         products.name = 'glib'
        )";
}


# add other arbitrary keywords
 if ($cgi->param('keywords')){
     $query .= ' AND bugs.keywords IN (' . 
               $dbh->quote(join(',', $cgi->param('keywords'))) . ')';
 }

# End build up $query string
my $bugs = $dbh->selectcol_arrayref($query);

if (!scalar @$bugs) {
    print "No bugs filed today\n";
    die;
}

my $buglist = Param("urlbase") . 'buglist.cgi?bug_id=' .join(",", @$bugs);

print<<FIN;
<HEAD>
<meta http-equiv="refresh" content="1; url=$buglist">
</HEAD>
FIN

