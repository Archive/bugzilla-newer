#!/usr/bin/perl -wT
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# weekly-bug-summary.cgi
#
# Display some nice bug stats for the Gnome weekly summary.
#
# Some of the functions can be passed the HTML or XML option.
# We use XML for emailing the Gnome weekly summary guys.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#   - Some products have way too many bugs. Break it down via component.
#   - Use percentages for the diff figures.

use strict;
use lib ".";

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use Bugzilla; 
require "CGI.pl";

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $template = Bugzilla->template;
my $vars = {};

require "globals.pl";
require "weekly-summary-utils.pl";

my $filename = "weekly-bug-summary.html";

my $format = GetFormat("reports/weekly-bug-summary", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));
                       
#print $cgi->header(-content_disposition => "inline; filename=$filename");
print $cgi->header($format->{'ctype'});


# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
Bugzilla->login();

# make sensible defaults.
my $version = undef;               # Don't limit to "2.7/2.8" or "2.5/2.6", etc.
my $days = 7;           # Change this if defn of week changes.
my $products = 15;      # Show the top 15 products.
my $hunters = 15;       # Show the top 15 hunters 
my $reporters = 15;     # Show the top 15 reporters
my $patchers = 10;      # Show the top 10 patchers
my $reviewers = 10;     # Show the top 10 reviewers
my $keyword = undef;    # Don't limit to any keyword.
my $classification=undef;   # Don't limit to just one classification.
my $product = undef;    # Don't limit to just one product.
my $links = "yes";      # Create links (can get very large sometimes)

if (defined $cgi->param('days') && $cgi->param('days') ne ""){
    $days = $cgi->param('days');
    detaint_natural($days) || die "days parameter must be a number";
}

if (defined $cgi->param('products') && $cgi->param('products') ne ""){
    $products = $cgi->param('products');
    detaint_natural($products) || die "products parameter must be a number";
}

if (defined $cgi->param('hunters') && $cgi->param('hunters') ne ""){
    $hunters = $cgi->param('hunters');
    detaint_natural($hunters) || die "hunters parameter must be a number";
}

if (defined $cgi->param('reporters') && $cgi->param('reporters') ne ""){
    $reporters = $cgi->param('reporters');
    detaint_natural($reporters) || die "reporters parameter must be a number";
}

if (defined $cgi->param('patchers') && $cgi->param('patchers') ne ""){
    $patchers = $cgi->param('patchers');
    detaint_natural($patchers) || die "patchers parameter must be a number";
}

if (defined $cgi->param('reviewers') && $cgi->param('reviewers') ne ""){
    $reviewers = $cgi->param('reviewers');
    detaint_natural($reviewers) || die "reviewers parameter must be a number";
}

if (defined $cgi->param('links')){
    $links = $cgi->param('links');
}

if (defined $cgi->param('keyword')){
    $keyword = $cgi->param('keyword');
    trick_taint($keyword);
}

if (defined $cgi->param('version') && $cgi->param('version') ne ""){
    $version = $cgi->param('version');
    trick_taint($version);
}

if (defined $cgi->param('classification') && $cgi->param('classification') ne ""){
    $classification = get_classification_id($cgi->param('classification'));
}

if (defined $cgi->param('product') && $cgi->param('product') ne ""){
    $product = get_product_id($cgi->param('product'));
    $products = 0;
    $classification = undef;
}

if ($days >= 90) {
    $links = 'no';
}

my $totalbugs = &get_total_bugs_on_bugzilla($keyword, $version, 
                                            $classification, $product);

my ($bugs_opened, $opened_buglist) = &bugs_opened($days, $keyword, $version,
                                                  $classification, $product);
my ($bugs_closed, $closed_buglist) = &bugs_closed($days, $keyword, $version,
                                                  $classification, $product);

#if ($links eq "yes") {
#    print "<a href=\"$buglist\">$bugs_closed</a> reports closed";
#} else {
#    print "$bugs_closed reports closed";
#}

my ($productlist) =
    &get_product_bug_lists($products, $days, $keyword, $links, $version,
                           $classification);

my ($hunterlist) = 
    &get_bug_hunters_list($hunters, $days, $keyword, 
                          $links, $version, $classification, $product);

my ($reporterlist) =
    &get_bug_reporters_list($reporters, $days, $keyword, 
                            $links, $version, $classification, $product);

my ($patchsubmitterlist) = 
    &get_patch_submitters_list($patchers, $days, $keyword,
                               $links, $version, $classification, $product);

my ($patchreviewerlist) = 
    &get_patch_reviewers_list($reviewers, $days, $keyword,
                              $links, $version, $classification, $product);

# CGI params / defaults:
$vars->{'days'} = $days;
$vars->{'keyword'} = $keyword;
$vars->{'links'} = $links;
$vars->{'version'} = $version;
$vars->{'classification'} =
    $classification ? get_classification_name($classification) : undef;;
$vars->{'product'} = $product ? get_product_name($product) : undef;
$vars->{'products'} = $products;
$vars->{'hunters'} = $hunters;
$vars->{'reporters'} = $reporters;
$vars->{'patchers'} = $patchers;
$vars->{'reviewers'} = $reviewers;

# Retrieved from queries:
$vars->{'totalbugs'} = $totalbugs;
$vars->{'openbugs'} = $bugs_opened;
$vars->{'openbuglist'} = $opened_buglist;
$vars->{'closedbugs'} = $bugs_closed;
$vars->{'closedbuglist'} = $closed_buglist;
$vars->{'productlist'} = $productlist;
$vars->{'hunterlist'} = $hunterlist;
$vars->{'reporterlist'} = $reporterlist;
$vars->{'patcherlist'} = $patchsubmitterlist;
$vars->{'reviewerlist'} = $patchreviewerlist;


$template->process("$format->{'template'}", $vars)
  || ThrowTemplateError($template->error());

