#!/usr/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# unconfirmed-bugs.cgi
#
# Display the product-components with the most unconfirmed bugs.
# A good place for bug hunters to start triaging.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#


use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

require "globals.pl";
require "CGI.pl";

use vars qw($template $vars @legal_product);

use Bugzilla;
my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $sth;

my $filename = "bugzilla_report.html";
print $cgi->header(-content_disposition => "inline; filename=$filename");

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
Bugzilla->login();

# If they didn't tell us how many choose 10.
my $number = 10;

if (defined $cgi->param('number')){
	$number = $cgi->param('number');
	detaint_natural($number) || die "number parameter must be a number";
}
PutHeader("Gnome Bugzilla - Unconfirmed Bugs");

# Always spew informative messages.
print "<p>Here is a list of Gnome Bugzilla products with the most UNCONFIRMED bugs. You can help triage these bugs by going through the list of unconfirmed bugs and seeing if they are real bugs or not.";

print "<p>If you find that the UNCONFIRMED bug exists on your system, then confirm the bug. You should also add extra comments on how to reproduce the bug, or a stack trace from the crash.";

print "<p>If you find an UNCONFIRMED bug that you know for sure is fixed in a later version of the program, then you should close the bug. But make sure you leave a comment in the bug about which version of the program fixes the bug, you could even cut and paste the relevant ChangeLog or NEWS entry which describes that bug being fixed.";

print "<p>If you notice similar UNCONFIRMED bugs then you should mark them as duplicates. Be careful to mark all the duplicates in the same report. You might want to look in the <a href=\"product-mostfrequent.cgi\">most-frequently reported list</a> for your module to see if there is already a report that is collecting duplicates for that bug. (the #bugs channel on irc.gnome.org is also a good place to ask if anyone has started marking duplicates for a particular bug)";

print "<p> NOTE: For each product, we limit to the top 10 components for that product.";

&print_unconfirmed_bug_list($number);

print "<p> You can also call this script with the parameter 'number', to get the reports to extend for a different number of products. For example: <a href=\"/reports/unconfirmed-bugs.cgi?number=20\">unconfirmed-bugs.cgi?number=20</a> will give the reports for the top 20 products, instead of the default 10 products.";

print "<p> If you spot any errors in this page please report it to <a href=\"mailto:bugmaster\@gnome.org\">bugmaster\@gnome.org</a>. Thanks.</p>";

PutFooter();

sub print_unconfirmed_bug_list() {
	my($number) = @_;

	my $product_unconfirmed;

        GetVersionTable();

        my @products = GetEnterableProducts();

	# Loop through the enterable products counting how many unconfirmed bugs.
	$product_unconfirmed = &count_unconfirmed(\@products, $number);	

	# Lets list the unconfirmed bugs for the top $number.
	my $i = 0;
	foreach my $product (reverse sort 
			{$product_unconfirmed->{$a} <=> $product_unconfirmed->{$b}}
					keys (%$product_unconfirmed)) {
		&print_unconfirmed_by_component($product, 
					$product_unconfirmed->{$product});
		$i++;
		if ($i == $number) { return }; 
	}
}

# For a specific product, show the number of unconfirmed bugs under
# each component.
# Only show the top 10 components.
sub print_unconfirmed_by_component() {
	my($product, $total) = @_;
	my $query;
	my $count;
        my $url_product;
        my $url_component;

        $url_product = url_quote($product);

	$query = <<FIN;
SELECT
	components.name, count(bugs.bug_id) as n 
FROM
	bugs
INNER JOIN
        components ON bugs.component_id = components.id
INNER JOIN
        products ON bugs.product_id = products.id
WHERE
	bugs.bug_status = 'UNCONFIRMED'
AND
	products.name = '$product'

GROUP BY components.name
ORDER BY n DESC
LIMIT 10

FIN
	# Print a nice cross-referenced table of results.
	print "<h3> Unconfirmed bugs for <a href=\"../buglist.cgi?product=$url_product&bug_status=UNCONFIRMED\">$product</a> (Total: $total):</h3>";
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Component</th><th>Number of UNCONFIRMED bug reports</th></tr>\n";
	SendSQL ($query);
	while (my ($component, $count) = FetchSQLData()) {

        $url_component = url_quote($component);
        
print <<FIN;
        <tr>
	<td><a href="buglist.cgi?product=$url_product&component=$url_component&bug_status=UNCONFIRMED">$component</a></td>
	<td>$count</td>
	</tr>
FIN

	}
	print "</table><p>\n";

}

sub count_unconfirmed()	{
	my ($products, $total) = @_;
	my $query;
	my $count;

        my ($quoted_products) = join(',', map { $dbh->quote($_) } @$products);
        my %prodcount;

	$query = <<FIN;
SELECT
	products.name, COUNT(bugs.bug_id) AS unco_bugs
FROM 
	bugs
INNER JOIN
        products ON bugs.product_id = products.id
WHERE 
	bugs.bug_status = 'UNCONFIRMED'
AND
	products.name IN ($quoted_products)
GROUP BY products.name
ORDER BY unco_bugs DESC
LIMIT $total
FIN
	$sth = $dbh->prepare($query);
        $sth->execute();
	
        while (my ($productname, $uncobugs) = $sth->fetchrow_array()) {
		$prodcount{$productname} = $uncobugs;
	}

	return(\%prodcount);
}
