#!/usr/bin/perl -wT
#
# Copyright 2006, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use Bugzilla; 
require "CGI.pl";

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $template = Bugzilla->template;
my $vars = {};

require "globals.pl";

my $filename = "points.html";
my $format = GetFormat("reports/points",
                       scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));
print $cgi->header($format->{'ctype'});

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
Bugzilla->login();

  # Create and run the query
  my $query = "
    SELECT
      points, count(*) AS num
    FROM
      profiles
    GROUP BY
      points
    ORDER BY
      points";

  $vars->{'order'} = $dbh->selectall_arrayref($query, {'Slice' => {}});

  $template->process("$format->{'template'}", $vars)
    || ThrowTemplateError($template->error());
