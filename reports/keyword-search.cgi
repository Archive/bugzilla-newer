#!/usr/bin/perl -w
#
# Copyright 2005, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";
 
require "CGI.pl";

require "globals.pl";
require "keyword-search-utils.pl";

use POSIX qw(ceil floor);

use Bugzilla;
Bugzilla->login();
my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;

use vars qw(@legal_product); # globals from er, globals.pl


# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
#quietly_check_login();

# make sensible defaults.
my $only_core_gnome=0;    # (boolean) whether to only show core gnome products
my $show_setter=1;        # (boolean) whether to show who set the keyword
my $product="%";          # (string) which product to search, % for all
my $keyword="gnome-love"; # (string) which keyword to search

if ($cgi->param('only_core_gnome')) {
  $only_core_gnome = $cgi->param('only_core_gnome');
}
if ($cgi->param('show_setter')) {
  $show_setter = $cgi->param('show_setter');
}
if ($cgi->param('product')) {
  $product = $cgi->param('product');
  $only_core_gnome = 0;  # The product might be outside core gnome...
}
if ($cgi->param('keyword')){
  $keyword = $cgi->param('keyword');
}

my $filename = "keyword-search-" . $keyword . ".html";
print $cgi->header(-content_disposition => "inline; filename=$filename");

PutHeader("$keyword bugs");

# Always spew informative messages.
print "<center><h1>";
if ($product ne '%') {
  print "$product bugs ";
} else {
  print "Bugs ";
}
print "with the " . $keyword . " keyword</h1></center>";

my $query = "
  SELECT
    description
  FROM
    keyworddefs
  WHERE
    name = ?
  ";
my ($keyword_desc) = $dbh->selectrow_array($query, undef, $keyword);

print "<p>This report shows bugs ";
if ($product ne '%') {
  print "in the $product module which are "
}
print "marked with the " . $keyword . " keyword.  That keyword is " .
      "used as follows: <blockquote>$keyword_desc</blockquote>  This " .
      "report tries to order bugs so that those bugs which have lots of " .
      "new comments or patches since the " . $keyword . " keyword was " .
      "added, or that have been around a long time since the keyword was " .
      "added, are pushed towards the bottom of the report.</p>\n";

if ($show_setter) {
  print "<p> The <b>who column</b> is the person who marked the bug as " .
        $keyword;
  if ($keyword eq 'gnome-love') {
    print ".  You should feel free to contact them if you have trouble " .
          "fixing the bug or need more info.  They aren't obligated to " .
          "walk you through it, but should be willing to provide a hint " .
          "or two.</p>\n";
  } else {
    print ".</p>";
  }
}

print "<p> The <b>stats column</b> provides some information that may be " .
      "of interest.  The three values are <i>number of new comments</i>, " .
      "<i>number of new patches</i>, and the <i>number of days</i> since " .
      "the bug was marked with the " . $keyword . " keyword.</p>\n";

# Start printing the table...
print "<table border=1 cellspacing=0 cellpadding=5>\n";
print "<tr>
       <th>Bug#</th>";
if ($product eq "%") {
  print "<th>Product</th>";
}
print "<th>Description</th>";
if ($show_setter) {
  print "<th>Who</th>";
}
print "
       <th>Stats</th>
       </tr>\n";

my @data = get_keyword_bug_info($only_core_gnome, $product, $keyword);

sub intelligently {
 my $aval = $a->{patch_count}*10 + $a->{comment_count} + floor($a->{age}/365);
 my $bval = $b->{patch_count}*10 + $b->{comment_count} + floor($b->{age}/365);
 return $aval     <=> $bval     ||
        $a->{age} <=> $b->{age} ||
        $b->{id}  <=> $a->{id};
}

my $total = 0;
foreach my $bug (sort intelligently @data) {
  # Trim the length of the name
  #if (length($who) > 19) {
  #  $who = substr($who, 0, 17) . "...";
  #}

  print "<tr>";
  print "<td> <a href=\"http://bugzilla.gnome.org/show_bug.cgi?id=" .
        "$bug->{id}\"> $bug->{id}</a> </td>";
  if ($product eq "%") {
    print "<td> $bug->{product} </td>";
  }
  print "<td> $bug->{description} </td>";
  if ($show_setter) {
    print "<td style=\"font-size:small;\"> $bug->{who} </td>";
  }
  printf "<td rowspan=2 valign=\"top\" align=\"right\"><pre>%d&nbsp;comments\n%d&nbsp;patches\n%d&nbsp;days</pre></td>", 
         $bug->{comment_count}, $bug->{patch_count}, $bug->{age};
  print "</tr>\n";

  print "<tr>";
  print "<td></td>";
  print "<td colspan=3><pre>" . 
        quoteUrlsOutsideBug($bug->{id}, substr($bug->{comment}, 0, 400)) .
        "</pre></td>";
  print "</tr>\n";

  $total++;
}

print "</table>\n";

print "<p>Total number of open " . $keyword . " bugs";
if ($only_core_gnome) {
  print " in core Gnome";
}
print ": $total</p>\n";

print "<!--" .
  "<p> SOME USEFUL PARAMETERS TO PASS THIS SCRIPT: " .
  "<ul>\n" .
  "<li>only_core_gnome (boolean, only show core gnome products)</li>\n" .
  "<li>show_setter     (boolean, whether to show who set the keyword)</li>\n" .
  "<li>product         (string,  which product to search in, % for all)</li>\n" .
  "<li>keyword         (string,  which keyword to search)</li>\n" .
  "</ul></p>\n" .
  "-->\n";

PutFooter();
