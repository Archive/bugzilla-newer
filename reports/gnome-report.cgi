#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Harrison Page <harrison@netscape.com>,
# Terry Weissman <terry@mozilla.org>,
# Dawn Endico <endico@mozilla.org>
# Bryce Nesbitt <bryce@nextbus.COM>,
#    Added -All- report, change "nobanner" to "banner" (it is strange to have a
#    list with 2 positive and 1 negative choice), default links on, add show
#    sql comment.
# Joe Robins <jmrobins@tgix.com>,
#    If using the usebuggroups parameter, users shouldn't be able to see
#    reports for products they don't have access to.
# Gervase Markham <gerv@gerv.net> and Adam Spiers <adam@spiers.net>
#    Added ability to chart any combination of resolutions/statuses.
#    Derive the choice of resolutions/statuses from the -All- data file
#    Removed hardcoded order of resolutions/statuses when reading from
#    daily stats file, so now works independently of collectstats.pl
#    version
#    Added image caching by date and datasets
# Myk Melez <myk@mozilla.org):
#    Implemented form field validation and reorganized code.
#
# Luis Villa <louie@ximian.com>:
# modified from evolution to gnome2 stuff
#
#    TODO:  sort component table by product then by bug totals
#           sorting ATM is by total # of bugs
#           needs to have a 'total' column at the bottom.
#
#
# Olav Vitters:
#    Cleaned up a little, ported to 2.20 and templified

use strict;

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use vars qw($vars);

require "globals.pl";

use Bugzilla;

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
Bugzilla->login();

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product @legal_gnome_version); # globals from er, globals.pl

# default for the moment 
my $version = "2.13/2.14";
my $quoted_version;
my @status = qw (NEW ASSIGNED REOPENED);
my @products;

# FIXME: These are 'priorities' in b.g.o., not 'severities' as in b.x.c.
my @severities = qw (Immediate Urgent High Normal Low);
my %bugs_severity;
my %bugs_per_item;

my @custom_keywords = qw (gnome-love accessibility string keynav);
my %custom_keyword_bugs;

my %bugs_per_product ;
my %bugs_per_component;
my %bugs_per_developer;

my %resolutions_per_developer;
my %resolutions_per_product;
my %fixes_per_developer;

GetVersionTable();

if (defined $cgi->param('version') && $cgi->param('version') ne ""){
    $version = $cgi->param('version');
    if (lsearch(\@::legal_gnome_version, $version) == -1) {
        die "Unknown GNOME version";
    }
    trick_taint($version);
}

# while this looks odd/redundant, it allows us to name
# functions differently than the value passed in

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.


my $filename = "gnome-report.html";
my $format = GetFormat("reports/gnome",
                       scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));
print $cgi->header($format->{'ctype'});

my ($products_list, $bug_severity, $custom_keyword, $bugs_per_item) = gnome_report($version);

$vars->{'product_order'} = $products_list;
$vars->{'severity_list'} = $bug_severity;
$vars->{'keyword_list'} = $custom_keyword;
$vars->{'keyword_order'} = \@custom_keywords;
$vars->{'version'} = $version;
$vars->{'severity_order'} = \@severities;
$vars->{'total_bugs'} = $bugs_per_item;

$template->process("$format->{'template'}", $vars)
  || ThrowTemplateError($template->error());


sub sort_by_total {
    ($bugs_severity{$a}{'High'} <=> $bugs_severity{$b}{'High'}) ||
    ($bugs_per_item{$a} <=> $bugs_per_item{$b})
}

sub gnome_report {
    my ($gnome_version) = @_;

    my $gnome_classifications = join(",", map { $dbh->quote($_) } GnomeClassifications());

    #FIXME: ponder including unconfirmed here
    my $query = 
           "SELECT bugs.bug_id, bugs.priority,
                   products.name AS product, components.name AS component, bugs.keywords
              FROM bugs
        INNER JOIN products
                ON bugs.product_id = products.id
        INNER JOIN components
                ON bugs.component_id = components.id
        INNER JOIN classifications
                ON products.classification_id = classifications.id
             WHERE bugs.gnome_version = ?
               AND classifications.name IN ($gnome_classifications)
               AND (bugs.bug_status = 'NEW' OR
                    bugs.bug_status = 'ASSIGNED' OR
                    bugs.bug_status = 'REOPENED')";
   
    my $thedata = $dbh->selectall_arrayref($query, undef, $gnome_version);
    
    my $c = 0;

    my $bugs_reopened = 0;
    my $product;
    my $custom_keyword;
    my $severity;
    my @products_list;
    push @products_list, 'Total';
    my %bugs_totals;
    my %bugs_lookup;

    # See the query string above for the field order:
    foreach my $rowRef (@$thedata) {
        my ($bid, $pri, $prod, $comp, $keywords) = @$rowRef;

        if(!exists($bugs_per_item{$prod})) #god this is a hack
        {
            $bugs_severity{$prod}{'High'}=0;
            foreach $custom_keyword (@custom_keywords) {
                $custom_keyword_bugs{$prod}{$custom_keyword}=0;
            }
        }

        $bugs_lookup{$bid} ++;

        $bugs_severity{$prod}{$pri} ++;
        $bugs_severity{'Total'}{$pri} ++;
        $bugs_severity{$prod}{'Total'} ++;

        $bugs_per_item{$prod} ++;
        $bugs_per_item{'Total'} ++;

        foreach $custom_keyword (@custom_keywords) {
            if($keywords =~ /\Q$custom_keyword\E/) {
                $custom_keyword_bugs{$custom_keyword}{$prod} ++;
                $custom_keyword_bugs{$custom_keyword}{'Total'} ++;
                #print("Add one to " . $custom_keyword . " - " . $prod . "<br>\n");
            }
        }
          
        if($bugs_per_item{$prod}==1) #sort of hackish way of only showing really buggy products
        {
            push @products_list, $prod;
        }
    }

    #here we hopefully loop out the table
    @products_list = reverse sort sort_by_total @products_list;
    
    return (\@products_list, \%bugs_severity, \%custom_keyword_bugs, \%bugs_per_item);
    
}

