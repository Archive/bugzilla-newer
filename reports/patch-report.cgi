#!/usr/bin/perl -wT

# (c) Copyright Elijah Newren 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use vars qw($vars);

require "globals.pl";

use Bugzilla;

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
Bugzilla->login();

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;

my $query;

require "patch-report-utils.pl";

my $filename = "patch-report.html";
my $format = GetFormat("reports/patch",
                       scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));
print $cgi->header($format->{'ctype'});


#
# First, get all the variables needed
#
my $quoted_product="'%'";   # (string) which product to search, % for all
my $quoted_component="'%'"; # (string) which component to search, % for all
my $patch_status="none";  # (string) status of patches to search for
my $min_days = -1;    # (int) Don't show patches younger than this (in days)
my $max_days = -1;    # (int) Don't show patches older than this (in days)
my $submitter;        # (int) submitter id

my @products = $cgi->param('product');
my @components = $cgi->param('component');

@products = grep { defined $_ && $_ ne "" } @products;
@components = grep { defined $_ && $_ ne "" } @components;

if (scalar @products) {
    $quoted_product = join(',', map($dbh->quote($_), @products));
}
if (scalar @components) {
    $quoted_component = join(',', map($dbh->quote($_), @components));
}

if (defined $cgi->param('patch-status') && $cgi->param('patch-status') ne ""){
    $patch_status = $cgi->param('patch-status');
}
if (defined $cgi->param('min_days') && $cgi->param('min_days') ne ""){
    $min_days = $cgi->param('min_days');
    detaint_natural($min_days) || die "min_days parameter must be a number";
}
if (defined $cgi->param('max_days') && $cgi->param('max_days') ne ""){
    $max_days = $cgi->param('max_days');
    detaint_natural($max_days) || die "max_days parameter must be a number";
}
if (defined $cgi->param('submitter') && $cgi->param('submitter') ne "") {
    $submitter = DBNameToIdAndCheck($cgi->param('submitter'));
}

# Determine the report type...
my $type;
if ($patch_status eq "none") {
    $type = "Unreviewed";
}
elsif ($patch_status eq "obsolete") {
    $type = "Obsolete";
}
else {
    # GetStatusID will verify the status
    GetStatusID($patch_status);
    $type = $patch_status;
}

#
# Second, set the report printing up...
#

#print "
#  <center>
#  <h1>$type Patches</h1>
#  </center>
#  ";

#print "\n";

#
# Third, collect the needed information
#
my $stats = get_unreviewed_patches_and_stats($quoted_product, 
                                             $quoted_component,
                                             $patch_status,
                                             $min_days,
                                             $max_days,
                                             $submitter);

#
# Finally, print it all out
#
$vars->{'patch_type'} = $type;
$vars->{'stats'} = $stats;

$template->process("$format->{'template'}", $vars)
  || ThrowTemplateError($template->error());


# Print it all out in table-like fashion
#foreach my $product (@{$stats->{product_list}}) {
#  print "<h3>$product->{name}<a name=\"$product->{name}\"></a> " .
#        "($product->{count})</h3>\n";
#  print "<ul>\n";
#  foreach my $component (@{$product->{component_list}}) {
#    print "<li>Component: $component->{name} ($component->{count})</li>\n";
#    $component->{maintainer} =~ s/[@.]/\ /g;
#    #print "<li>$component->{maintainer}</li>\n";
#    print "<ul>\n";
#    foreach my $bug (@{$component->{bug_list}}) {
#      print "<li>Bug <a href=\"http://bugzilla.gnome.org/show_bug.cgi?" .
#            "id=$bug->{id}\">$bug->{id}</a>: $bug->{summary}" .
#            " ($bug->{priority}/$bug->{severity})</li>\n";
#      print "<ul>\n";
#      foreach my $patch (@{$bug->{patch_list}}) {
#        print "<li><i>Attachment $patch->{id}</i> ($patch->{age} days) " .
#              "<a href=\"http://bugzilla.gnome.org/attachment.cgi?" .
#              "id=$patch->{id}&action=edit\">$patch->{description}</a>" .
#              "</li>\n";
#      }
#      print "</ul>\n";
#    }
#    print "</ul>\n";
#  }
#  print "</ul>\n";
#}
#if ($stats->{count} > 0) {
#  print "<p>$stats->{count} " . lc($type) . " patches found.</p>\n";
#} else {
#  print "<p>No patches found matching your criteria.</p>\n";
#}
#
##
## Don't forget to provide some useful information...
##
#print "<!--" .
#  "<p> SOME USEFUL PARAMETERS TO PASS THIS SCRIPT: <ul>\n" .
#  "<li>product   (Only show unreviewed patches in this product)</li>\n" .
#  "<li>component (Only show unreviewed patches in this component)</li>\n" .
#  "<li>min_days  (Don't show patches younger than this)</li>\n" .
#  "<li>max_days  (Don't show patches older than this)</li>\n" .
#  "</ul></p>\n" .
#  "-->\n";
