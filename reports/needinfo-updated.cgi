#!/usr/bin/perl -wT
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# needinfo-updated.cgi
#
# Tell us which NEEDINFO reports have been given updated info.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#	- Show stale NEEDINFO list as well? (eg: for mass closure)


use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

require "CGI.pl";
use vars qw($template $vars); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

use Bugzilla;
Bugzilla->login();
my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;

my $query;
my $sth;

# Output appropriate HTTP response headers
my $filename = "bugzilla_report.html";
print $cgi->header(-content_disposition => "inline; filename=$filename");

PutHeader("NEEDINFO Reports Requiring Attention");

# Always spew informative messages.
print "<p>When a bug report is filed that is lacking in info, the Gnome bughunters or the module maintainers will mark the bug as NEEDINFO.";
print "<p>This page gives a list of bugs which have been updated since the NEEDINFO status was sent. This is hopefully because the original reported has replied with the requested information. (Often it will just be bughunters making comments or marking duplicates - we haven't figured out how to not include these yet)</p>";

# If no parameters are passed we list all the reports for all products.
if (! defined $cgi->param('product')){
	print_needinfo_updates();
} else {
	my $product = $cgi->param('product');
	print_needinfo_updates($product);
}

print "<p> If you spot any errors in this page please report it to <a href=\"mailto:bugmaster\@gnome.org\">bugmaster\@gnome.org</a>. Thanks.</p>";

PutFooter();


# print_needinfo_updates(product);
#
# Parameters:
#	product = (string) bugzilla product name
#
#
#
sub print_needinfo_updates {
	my($product) = @_;

	my $query;
	my $query_resolved;

	# We are going to build a long SQL query.
	# Attempt to see find a bugs_activity entry where NEEDINFO was marked
	# and that the current bug modification is newer than this date.
	# We give an extra 15 minutes grace, because the bug hunter
	# may make some comments in that time, and we don't want to get those.
	# FIXME: This query could be improved somewhat. It lists many
	# examples of comments by bughunters or duplicates which we don't
	# want to see.
	$query = "
                SELECT bugs.bug_id, assign.login_name, bugs.short_desc, 
                       products.name, components.name
                  FROM bugs
            INNER JOIN bugs_activity
                    ON bugs.bug_id = bugs_activity.bug_id
            INNER JOIN profiles assign
                    ON bugs.assigned_to = assign.userid
            INNER JOIN products
                    ON bugs.product_id = products.id
            INNER JOIN components
                    ON bugs.component_id = components.id
                 WHERE bugs.bug_status = 'NEEDINFO'";
        if ($product) {
            $query .= "
                   AND bugs.product LIKE ?";
        }
        $query .= "
                   AND bugs_activity.added='NEEDINFO'
                   AND (bugs_activity.bug_when + INTERVAL 15 MINUTE) < bugs.lastdiffed
              GROUP BY bug_id
";

	# Print a nice cross-referenced table of results.
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Bug ID</th><th>Owner</th><th>Product</th><th>Summary</th></tr>\n";
	$sth = $dbh->prepare($query);
        if ($product) {
            $sth->execute($product);
        } else {
            $sth->execute();
        }
	while (my ($bug_id, $assigned_to, $summary, $product, $component) = $sth->fetchrow_array) {

print <<FIN;
        <tr>
	<td><a href="../show_bug.cgi?id=$bug_id">$bug_id</a></td>
	<td><a href="mailto:$assigned_to">$assigned_to</a></td>
	<td><a href="http://bugzilla.gnome.org/buglist.cgi?product=$product&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">$product</a>/<a href="http://bugzilla.gnome.org/buglist.cgi?product=$product&component=$component&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">$component</a></td>
	<td>$summary</td>
	</tr>
FIN
	}
	print "</table><p>\n";
}
