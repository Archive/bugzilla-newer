#!/usr/bin/perl -wT
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# check-assignedto.cgi
#
# Tell us 
#
# This file was based on the original needinfo-updated.cgi but has been
# modified by Olav Vitters (olav@bkor.dhs.org), Jan 2005.
#

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use vars qw($vars);

require "globals.pl";

use Bugzilla;

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
Bugzilla->login();

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;

my $query;

$query = "
        SELECT components.initialowner
          FROM products 
    INNER JOIN components
            ON products.id = components.product_id
    INNER JOIN profiles
            ON profiles.userid = components.initialowner
         WHERE products.disallownew = '0'
           AND profiles.login_name LIKE '%\@gnome.bugs'
      GROUP BY components.initialowner";

my $defaultowners = join(",", @{$dbh->selectcol_arrayref($query)});

$query = "
        SELECT bugs.bug_id, assigned.login_name, defaultowner.login_name,
               products.name, components.name, bugs.short_desc
          FROM bugs
    INNER JOIN components
            ON bugs.component_id = components.id
    INNER JOIN products
            ON bugs.product_id = products.id
    INNER JOIN profiles assigned
            ON bugs.assigned_to = assigned.userid
    INNER JOIN profiles defaultowner
            ON components.initialowner = defaultowner.userid
         WHERE (bugs.assigned_to != components.initialowner)
           AND bugs.bug_status IN ('UNCONFIRMED', 'NEW')
           AND bugs.assigned_to IN ($defaultowners)
           AND components.initialowner IN ($defaultowners)
      ORDER BY products.name ASC, bugs.bug_id ASC";

$vars->{'result'} = $dbh->selectall_arrayref($query);

# Generate and return the UI (HTML page) from the appropriate template.
print $cgi->header();
$template->process("reports/check-assignedto.html.tmpl", $vars)
  || ThrowTemplateError($template->error());

