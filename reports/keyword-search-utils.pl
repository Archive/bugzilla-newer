#!/usr/bin/perl -wT
#
# Copyright 2005, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

# TODO (outdated; may still have useful info)
#   Things to consider:
#     date marked as easy fix
#     whether it has patches
#     whether it has unreviewed or accepted patches
#     number of comments since the bug was marked as easy fix
#     what was the comment that accompanied the keyword being added
#   Formats:
#     just a big html page with all the above info in a table?
#     some kind of blog-like thing that Bryan was suggesting?
#   Other points to ponder:
#     Ways to fight misuse of keyword (lart people who misuse it, and
#       maybe even make a query that points out possible misuses, by
#       perhaps searching bugs_activity.who and seeing if their marked
#       as a gnome hacker--or checking the wording of the comment somehow)

use strict;

require "CGI.pl";

require "globals.pl";

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;
my $sth;

use POSIX qw(ceil floor);

sub get_keyword_bug_info() {
  my ($only_core_gnome, $product, $keyword) = (@_);

  my $quoted_product = $dbh->quote($product);
  my $quoted_keyword = $dbh->quote($keyword);

  my $keywords_fieldid = GetFieldID('keywords');

  my $query = "
    SELECT
      bugs.bug_id, 
      bugs.creation_ts,
      " . $dbh->sql_to_days('NOW()') . "-" . $dbh->sql_to_days('bugs.creation_ts') . ",
      bugs.reporter,
      products.name,
      substring(bugs.short_desc, 1, 60)
    FROM
      bugs
    INNER JOIN
      products ON bugs.product_id = products.id
    INNER JOIN
      keywords ON bugs.bug_id = keywords.bug_id
    INNER JOIN
      keyworddefs ON keywords.keywordid = keyworddefs.id
    WHERE
      keyworddefs.name = ?
    AND
      (
       bugs.bug_status='UNCONFIRMED' OR
       bugs.bug_status='NEW' OR
       bugs.bug_status='ASSIGNED' OR
       bugs.bug_status='REOPENED'
      )
    AND
      products.name LIKE ?";

  $sth = $dbh->prepare($query);
  $sth->execute($keyword, $product);

  my @temp_data = ();
  my @data = ();

  while (my ($bug_id, $bug_creation, $bug_age, $bug_who, $bug_product, $bug_desc) = $sth->fetchrow_array) {
    push @temp_data, { id       => $bug_id, 
                       creation => $bug_creation,
                       age      => $bug_age,
                       who      => $bug_who,
                       product  => $bug_product,
                       desc     => $bug_desc };
  }

  foreach my $bug (@temp_data) {
    #
    # First, try to find the last time the keyword was added
    #
    my $when = $bug->{creation};
    my $age  = $bug->{age};
    my $who  = $bug->{who};
    my $bug_product = $bug->{product};
    my $desc = $bug->{desc};
    $query = "
      SELECT
        bugs_activity.bug_when,
        " . $dbh->sql_to_days('NOW()') . "-" . $dbh->sql_to_days('bugs_activity.bug_when') . ",
        bugs_activity.who
      FROM
        bugs_activity
      WHERE
        bugs_activity.bug_id = ?
      AND
        bugs_activity.fieldid = ?
      AND
        INSTR(bugs_activity.added,?)
      ORDER BY
        bugs_activity.bug_when DESC
      LIMIT
        1";
    my ($change_when, $change_age, $change_who) = $dbh->selectrow_array($query, undef, $bug->{id}, $keywords_fieldid, $keyword);
    if ($change_when && $change_age < $age) {
      $age = $change_age;
      $when = $change_when;
      $who  = $change_who;
    }

    #
    # Second, find the number of comments since the bug had the
    # keyword added
    #
    $query = "
      SELECT
        count(longdescs.bug_id)
      FROM
        longdescs
      WHERE
        longdescs.bug_id = ?
      AND
        longdescs.bug_when > ?";
    my ($comment_count) = $dbh->selectrow_array($query, undef, $bug->{id}, $when);

    #
    # Third, find the number of attachments added after the keyword was
    #
    $query = "
      SELECT
        count(attachments.bug_id)
      FROM
        attachments
      WHERE
        attachments.bug_id = ?
      AND
        attachments.creation_ts > '$when'";
    my ($patch_count) = $dbh->selectrow_array($query, undef, $bug->{id});

    #
    # Fourth, find the comment that was added at the same time the bug was
    # marked with the keyword, if any
    #
    $query = "
      SELECT
        substring(longdescs.thetext, 1, 5000)
      FROM
        longdescs
      WHERE
        longdescs.bug_id = ?
      AND
        longdescs.bug_when = ?
      LIMIT 1";
    my ($comment_when_marked) = $dbh->selectrow_array($query, undef, $bug->{id}, $when);
    if (!defined($comment_when_marked)) {
      # Use the first comment instead
      $query = "
        SELECT
          substring(longdescs.thetext, 1, 5000)
        FROM
          longdescs
        WHERE
          longdescs.bug_id = ?
        ORDER BY
          longdescs.bug_when ASC
        LIMIT 1";
      $comment_when_marked = "Description:\n" . $dbh->selectrow_array($query, undef, $bug->{id});
    }

    my $query = "
      SELECT
        realname, login_name
      FROM
        profiles
      WHERE
        profiles.userid = ?";
    my ($realname, $login_name) = $dbh->selectrow_array($query, undef, $who);
    $login_name =~ s/[@.]/\ /g;
    $realname ||= $login_name;

    push @data, { id            => $bug->{id},
                  product       => $bug_product,
                  description   => $desc,
                  comment_count => $comment_count,
                  patch_count   => $patch_count,
                  age           => $age,
                  comment       => $comment_when_marked,
                  who           => $realname };
  }

  return @data;
}

# Rip-off of quoteUrls from globals.pl with some minor changes.
sub quoteUrlsOutsideBug {
    my ($bug_id, $text) = (@_);
    return $text unless $text;
    
    my $base = Param('urlbase');

    my $protocol = join '|',
    qw(afs cid ftp gopher http https mid news nntp prospero telnet wais);

    my $count = 0;

    # Now, quote any "#" characters so they won't confuse stuff later
    $text =~ s/#/%#/g;

    # Next, find anything that looks like a URL or an email address and
    # pull them out the the text, replacing them with a "##<digits>##
    # marker, and writing them into an array.  All this confusion is
    # necessary so that we don't match on something we've already replaced,
    # which can happen if you do multiple s///g operations.

    my @things;
    while ($text =~ s%((mailto:)?([\w\.\-\+\=]+\@[\w\-]+(?:\.[\w\-]+)+)\b|
                    (\b((?:$protocol):[^ \t\n<>"]+[\w/])))%"##$count##"%exo) {
        my $item = $&;

        $item = value_quote($item);

        if ($item !~ m/^$protocol:/o && $item !~ /^mailto:/) {
            # We must have grabbed this one because it looks like an email
            # address.
            $item = qq{<A HREF="mailto:$item">$item</A>};
        } else {
            $item = qq{<A HREF="$item">$item</A>};
        }

        $things[$count++] = $item;
    }
    # Either a comment string or no comma and a compulsory #.
    while ($text =~ s/\bbug(\s|%\#)*(\d+),?\s*comment\s*(\s|%\#)(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $bugnum = $2;
        $bugnum = $bug_id unless $bugnum;
        my $comnum = $4;
        $item = GetBugLink($bugnum, $item);
        $item =~ s/(id=\d+)/$1#c$comnum/;
        $things[$count++] = $item;
    }
    while ($text =~ s/\bcomment(\s|%\#)*(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $num = $2;
        $item = value_quote($item);
        $item = qq{show_bug.cgi?id=$bug_id#c$num">$item</A>};
        $things[$count++] = $item;
    }
    while ($text =~ s/\bbug(\s|%\#)*(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $num = $2;
        $item = GetBugLink($num, $item);
        $things[$count++] = $item;
    }
    while ($text =~ s/\b(Created an )?attachment(\s|%\#)*(\(id=)?(\d+)\)?/"##$count##"/ei) {
        my $item = $&;
        my $num = $4;
        $item = value_quote($item); # Not really necessary, since we know
                                    # there's no special chars in it.
        $item = qq{<a href="attachment.cgi?id=$num&amp;action=view">$item</a>};
        $things[$count++] = $item;
    }
    while ($text =~ s/\*\*\* This bug has been marked as a duplicate of (\d+) \*\*\*/"##$count##"/ei) {
        my $item = $&;
        my $num = $1;
        my $bug_link;
        $bug_link = GetBugLink($num, $num);
        $item =~ s@\d+@$bug_link@;
        $things[$count++] = $item;
    }

    $text = value_quote($text);
    $text =~ s/\&#013;/\n/g;

    # Stuff everything back from the array.
    for (my $i=0 ; $i<$count ; $i++) {
        $text =~ s/##$i##/$things[$i]/e;
    }

    # And undo the quoting of "#" characters.
    $text =~ s/%#/#/g;

    return $text;
}

# Let perl know we ended okay
1;
