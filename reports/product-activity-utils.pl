#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

# (c) Copyright Elijah Newren 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use strict;

require "CGI.pl";
require "globals.pl";

sub get_total_patch_count {
    my ($min_days, $max_days, $product) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;

    $query =    "SELECT products.name, COUNT(attach_id) as N
                   FROM attachments
              LEFT JOIN bugs ON attachments.bug_id = bugs.bug_id
              LEFT JOIN products ON bugs.product_id = products.id
                  WHERE ispatch = 1
                    AND isobsolete = 0
                    AND attachments.creation_ts <= NOW() - " .
                            $dbh->sql_interval($min_days, 'DAY') . "
                    AND attachments.creation_ts >= NOW() - " .
                            $dbh->sql_interval($max_days, 'DAY');

    if ($product) {
        my $quoted_product = $dbh->quote($product);
        $query .= "   AND products.name = $quoted_product\n";
    }
    $query .= " GROUP BY product_id
                ORDER BY N";

    my $patch_count = $dbh->selectall_arrayref($query);
    return $patch_count;
}

sub get_unreviewed_patch_count {
    my ($min_days, $max_days, $product) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;

    $query =    "SELECT products.name, COUNT(attach_id) as N
                   FROM attachments
              LEFT JOIN bugs ON attachments.bug_id = bugs.bug_id
              LEFT JOIN products ON bugs.product_id = products.id
              LEFT JOIN flagtypes ON attachments.status_id = flagtypes.id
                  WHERE ispatch = 1
                    AND isobsolete = 0
                    AND flagtypes.name = 'none'
                    AND bug_status IN ('" .
                          (join "','", &::OpenStates())  . "')
                    AND attachments.creation_ts <= NOW() - " .
                          $dbh->sql_interval($min_days, 'DAY') . "
                    AND attachments.creation_ts >= NOW() - " .
                          $dbh->sql_interval($max_days, 'DAY');

    if ($product) {
      my $quoted_product = $dbh->quote($product);
      $query .= "   AND products.name = $quoted_product\n";
    }
    $query .= " GROUP BY product_id
                ORDER BY N";

    my $unreviewed_count = $dbh->selectall_arrayref($query);
    return $unreviewed_count;
}

sub get_patch_diligence_ratings {
    my ($min_days, $max_days, $product) = (@_);

    my $total_count = 
        &get_total_patch_count($min_days, $max_days, $product);
    my $unreviewed_count = 
        &get_unreviewed_patch_count($min_days, $max_days, $product);
    my %counts;
    foreach my $row (@$total_count) {
        $counts{$row->[0]}->{total} = $row->[1];
        $counts{$row->[0]}->{unreviewed} = 0;
    }
    foreach my $row (@$unreviewed_count) {
        $counts{$row->[0]}->{unreviewed} = $row->[1];
    }
    foreach my $product (keys %counts) {
        $counts{$product}->{rating} = 100 *
            (1 - $counts{$product}->{unreviewed}/$counts{$product}->{total});
        $counts{$product}->{reviewed} =
            $counts{$product}->{total} - $counts{$product}->{unreviewed};
    }

    return \%counts;
}

sub get_total_bug_count {
    my ($min_days, $max_days, $product) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;

    $query =    "SELECT products.name, COUNT(bug_id) as N
                   FROM bugs
              LEFT JOIN products ON bugs.product_id = products.id
                  WHERE bugs.creation_ts >= NOW() - " .
                            $dbh->sql_interval($max_days, 'DAY') . "
                    AND bugs.creation_ts <= NOW() - " .
                            $dbh->sql_interval($min_days, 'DAY');

    if ($product) {
      my $quoted_product = $dbh->quote($product);
      $query .= "   AND products.name = $quoted_product\n";
    }
    $query .= " GROUP BY product_id
                ORDER BY N";

    my $bug_count = $dbh->selectall_arrayref($query);
    return $bug_count;
}

sub get_unresponded_bug_count {
    my ($min_days, $max_days, $product) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;

    die "Product is mandatory, sorry.\n" if (!$product);
    my $quoted_product = $dbh->quote($product);

    my $dev_ids =
      $dbh->selectcol_arrayref("SELECT userid
                                  FROM developers, products
                                 WHERE developers.product_id = products.id
                                   AND products.name = ?",
                               undef, $quoted_product);
    $query =     "SELECT bugs.bug_id
                    FROM bugs, products
              INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id
                   WHERE bugs.product_id = products.id
                     AND bugs.product_id = $quoted_product
                     AND bug_severity != 'enhancement'
                     AND reporter NOT IN (" . 
                             (join ",", @$dev_ids) . ")
                     AND bug_status IN ('" .
                             (join "','", &::OpenStates())  . "')
                GROUP BY bugs.bug_id
                  HAVING COUNT(DISTINCT longdescs.who) = '1'";

    my $all_bugs = $dbh->selectcol_arrayref($query);
    my $unresponded_count = scalar(@$all_bugs);
    return $unresponded_count;
}

sub get_responsiveness_ratings {
    my ($min_days, $max_days, $product) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;

    my $sth_get_dev_ids =
      $dbh->prepare("SELECT userid
                       FROM developers, products
                      WHERE developers.product_id = products.id
                        AND products.name = ?");

    ## This doesn't work worth beans because the 
    ##   reporter NOT IN (?)
    ## line is too flaky; the ? can only bind a single value rather than an
    ## array.
    #my $sth_get_unresponded_bugs =
    #  $dbh->prepare("SELECT bugs.bug_id
    #                   FROM bugs, products
    #             INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id
    #                  WHERE bugs.product_id = products.id
    #                    AND products.name = ?
    #                    AND bug_severity != 'enhancement'
    #                    AND reporter NOT IN (?) 
    #                    AND bug_status IN ('" .
    #                            (join "','", &::OpenStates())  . "')
    #                    AND TO_DAYS(NOW()) - TO_DAYS(creation_ts) <= $max_days
    #                    AND TO_DAYS(NOW()) - TO_DAYS(creation_ts) >= $min_days
    #               GROUP BY bugs.bug_id
    #                 HAVING COUNT(DISTINCT longdescs.who) = '1'");

    my $total_count = 
        &get_total_bug_count($min_days, $max_days, $product);
    my %counts;
    foreach my $row (@$total_count) {
        my ($product, $bug_count) = @$row;

        $sth_get_dev_ids->execute($product);
        my $dev_ids = $sth_get_dev_ids->fetchall_arrayref([0]);
        my $devs_in_string = join(",", map { @$_ } @$dev_ids);
        my $has_devs = scalar(@$dev_ids);

        # This CANNOT be done as a separate prepare stage because a
        # "NOT IN (?)" could only allow the ? to represent a single value,
        # as I found out the hard way before seeing that it was on the
        # Perl::DBI man page.
        my $query =   "SELECT bugs.bug_id
                       FROM bugs
                 INNER JOIN products on bugs.product_id = products.id
                 INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id
                      WHERE products.name = " . $dbh->quote($product) . "
                        AND bug_severity != 'enhancement'\n";
        if ($has_devs) {
            $query .= " AND reporter NOT IN ($devs_in_string)\n";
        }
        $query .=     " AND bug_status IN ('" .
                                (join "','", &::OpenStates())  . "')
                        AND TO_DAYS(NOW()) - TO_DAYS(creation_ts) <= $max_days
                        AND TO_DAYS(NOW()) - TO_DAYS(creation_ts) >= $min_days
                   GROUP BY bugs.bug_id
                     HAVING COUNT(DISTINCT longdescs.who) = '1'";
        my $all_bugs = $dbh->selectcol_arrayref($query);
        my $unresponded_count = scalar(@$all_bugs);

        $counts{$product}->{total} = $bug_count;
        $counts{$product}->{unresponded} = $unresponded_count;
        $counts{$product}->{responded} = $bug_count - $unresponded_count;
        $counts{$product}->{rating} = 100 * 
            (1 - $unresponded_count / $bug_count);
    }

    return \%counts;
}

# Let perl know we ended okay
1;
