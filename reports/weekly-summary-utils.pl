#!/usr/bin/perl -wT
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# weekly-summary-utils.pl
#
# Utility library shared between weekly-bug-summary.cgi and email_gnome_summary_report.pl and others probably.
#
# Some of the functions can be passed the HTML or XML option.
# We use XML for emailing the Gnome weekly summary guys.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#   - Some products have way too many bugs. Break it down via component.
#   - Use percentages for the diff figures.

use strict;

require "CGI.pl";
use Bugzilla;
my $dbh = Bugzilla->dbh;

require "globals.pl";

my $urlbase = Param('urlbase');

sub get_total_bugs_on_bugzilla {
    my($keyword, $version, $classification_id, $product_id) = @_;

    my @args;

    my $query = "
     SELECT COUNT(bugs.bug_id)
       FROM bugs";

    if ($classification_id) {
        $query .= "
 INNER JOIN products
         ON bugs.product_id = products.id";
    }

    $query .= "
      WHERE (bugs.bug_status = 'NEW' OR bugs.bug_status = 'ASSIGNED'
             OR bugs.bug_status = 'REOPENED'
             OR bugs.bug_status = 'UNCONFIRMED')";

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
        AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
        AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
        AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
        AND bugs.product_id = ?";
    }

    my ($count) = $dbh->selectrow_array($query, undef, @args);
    return($count);
}

# bugs_closed
# Show how many bugs have been closed for $product_id in $days.
# Pass undef as product_id to get all products.
sub bugs_closed {
    my($days, $keyword, $version, $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
     SELECT DISTINCT bugs.bug_id
       FROM bugs
 INNER JOIN bugs_activity
         ON bugs.bug_id = bugs_activity.bug_id";

    if ($classification_id) {
        $query .= "
 INNER JOIN products
         ON bugs.product_id = products.id";
    }

    $query .= "
      WHERE bugs.bug_status IN ('RESOLVED','CLOSED','VERIFIED')
        AND bugs_activity.added IN ('RESOLVED','CLOSED')
        AND bugs_activity.bug_when >= NOW() - " .
            $dbh->sql_interval('?', 'DAY');

    push(@args, $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
        AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
        AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
        AND products.classification_id = ?";
    }
    if ($product_id) {
        $query .= "
        AND bugs.product_id = $product_id";
    }

    $query .= "
   GROUP BY bugs.bug_id";

    my $bugs = $dbh->selectcol_arrayref($query, undef, @args);

    # Create URL of all the bugs that match this function.
    my $buglist = "$urlbase/buglist.cgi?bug_id=" . join(",", @$bugs);
    my $count = scalar @$bugs;

    return(($count, $buglist));
}

# bugs_opened
# Show how many bugs have been opened for $product_id in $days.
# Pass undef as product_id to get all products.
sub bugs_opened {
    my($days, $keyword, $version, $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
     SELECT bugs.bug_id
       FROM bugs";
    
    if ($classification_id) {
        $query .= "
 INNER JOIN products
         ON bugs.product_id = products.id";
    }

    $query .= "
      WHERE bugs.creation_ts >= NOW() - " . $dbh->sql_interval('?', 'DAY');

    push(@args, $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
        AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
        AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
        AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
        AND bugs.product_id = ?";
    }

    $query .= "
      GROUP BY bugs.bug_id";

    my $bugs = $dbh->selectcol_arrayref($query, undef, @args);

    # Create URL of all the bugs that match this function.
    my $buglist = "$urlbase/buglist.cgi?bug_id=" . join(",", @$bugs);
    my $count = scalar @$bugs;

    return(($count, $buglist));
}

sub get_product_bug_lists {
    my($number, $days, $keyword, $links, $version, $classification_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
        SELECT bugs.product_id, products.name AS product, COUNT(bugs.bug_id) AS n 
          FROM bugs
    INNER JOIN products
            ON bugs.product_id = products.id
         WHERE (bugs.bug_status = 'NEW' OR bugs.bug_status = 'ASSIGNED' 
                OR bugs.bug_status = 'REOPENED'
                OR bugs.bug_status = 'UNCONFIRMED')
           AND bugs.bug_severity != 'enhancement'";

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
           AND products.classification_id = ?";
    }

    $query .= "
      GROUP BY bugs.product_id
      ORDER BY n DESC " . $dbh->sql_limit($number);

    my $productlist = $dbh->selectall_arrayref($query, undef, @args);

    foreach my $rowRef (@$productlist) {
        my ($product_id, $product, $count) = @$rowRef;

        my ($opened, $openedlist) = &bugs_opened($days, $keyword, $version,
                                                 $classification_id, $product_id);
        my ($closed, $closedlist) = &bugs_closed($days, $keyword, $version,
                                                 $classification_id, $product_id);

        my $change = $opened-$closed;

        push(@$rowRef, $opened, $openedlist, $closed, $closedlist, $change);
    }

    return $productlist;
}

sub get_bug_hunters_list {
    my($number, $days, $keyword, $links, $version,
       $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
        SELECT bugs_activity.who AS userid, COUNT(bugs.bug_id) as n 
          FROM bugs
    INNER JOIN bugs_activity
            ON bugs.bug_id = bugs_activity.bug_id";

    if ($classification_id) {
        $query .= "
    INNER JOIN products
            ON bugs.product_id = products.id";
    }

    $query .= "
         WHERE bugs.bug_status IN ('RESOLVED','CLOSED','VERIFIED')
           AND bugs_activity.added IN ('RESOLVED','CLOSED')
           AND bugs_activity.bug_when =
                 (SELECT MAX(bug_when)
                    FROM bugs_activity ba
                   WHERE ba.added IN ('RESOLVED','CLOSED')
                     AND ba.removed IN ('UNCONFIRMED','REOPENED',
                                        'NEW','ASSIGNED','NEEDINFO')
                     AND ba.bug_id = bugs_activity.bug_id)
           AND bugs_activity.bug_when >= NOW() - " .
                   $dbh->sql_interval('?', 'DAY');

    push(@args, $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
           AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
           AND bugs.product_id = ?";
    }

    $query .= "
      GROUP BY bugs_activity.who
      ORDER BY n desc " . $dbh->sql_limit($number);

    my $hunterlist = $dbh->selectall_arrayref($query, undef, @args);

    foreach my $rowRef (@$hunterlist) {
        my($userid, $count) = @$rowRef;
        
        push(@$rowRef, new Bugzilla::User($userid));
        if ($links eq "yes") {
            my $buglist = &get_hunter_bugs($userid, $days, $keyword, $version,
                                           $classification_id, $product_id);
            push(@$rowRef, $buglist);
        }
    }

    return $hunterlist;
}

sub get_hunter_bugs {
    my($userid, $days, $keyword, $version, $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
         SELECT DISTINCT bugs.bug_id
           FROM bugs
     INNER JOIN bugs_activity
             ON bugs.bug_id = bugs_activity.bug_id";

    if ($classification_id) {
        $query .= "
          INNER JOIN products
                  ON bugs.product_id = products.id";
    }

    $query .= "
         WHERE bugs.bug_status IN ('RESOLVED','CLOSED','VERIFIED')
           AND bugs_activity.added IN ('RESOLVED','CLOSED')
           AND bugs_activity.bug_when =
                 (SELECT MAX(bug_when)
                    FROM bugs_activity ba
                   WHERE ba.added IN ('RESOLVED','CLOSED')
                     AND ba.removed IN ('UNCONFIRMED','REOPENED',
                                        'NEW','ASSIGNED','NEEDINFO')
                     AND ba.bug_id = bugs_activity.bug_id)
            AND bugs_activity.bug_when >= NOW() - " .
                    $dbh->sql_interval('?', 'DAY') . "
            AND bugs_activity.who = ?";

    push(@args, $days, $userid);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
                 AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
           AND bugs.product_id = ?";
    }

    my $bugs = $dbh->selectcol_arrayref($query, undef, @args);

    my $buglist = "$urlbase/buglist.cgi?bug_id=" . join(",", @$bugs);

    return ($buglist);
}

sub get_bug_reporters_list {
    my($number, $days, $keyword, $links, $version,
       $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
        SELECT bugs.reporter AS userid, COUNT(DISTINCT bugs.bug_id) AS n 
          FROM bugs";

    if ($classification_id) {
        $query .= "
          INNER JOIN products
                  ON bugs.product_id = products.id";
    }

    $query .= "
         WHERE bugs.creation_ts >= NOW() - " . $dbh->sql_interval('?', 'DAY') . "
           AND NOT (bugs.bug_status = 'RESOLVED' AND 
                    bugs.resolution IN ('DUPLICATE','INVALID','NOTABUG',
                                        'NOTGNOME','INCOMPLETE'))";

    push(@args, $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
           AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
           AND bugs.product_id = ?";
    }
    
    $query .= "
      GROUP BY bugs.reporter
      ORDER BY n DESC " . $dbh->sql_limit($number);

    my $reporterlist = $dbh->selectall_arrayref($query, undef, @args);

    foreach my $rowRef (@$reporterlist) {
        my ($userid, $count) = @$rowRef;

        push(@$rowRef, new Bugzilla::User($userid));
        if ($links eq "yes") {
            my $buglist = &get_reporter_bugs($userid, $days, $keyword,
                                             $version, $classification_id,
                                             $product_id);
            push(@$rowRef, $buglist);
        }
    }

    return $reporterlist;
}

sub get_reporter_bugs() {
    my($userid, $days, $keyword, $version, $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
     SELECT bugs.bug_id
       FROM bugs";

    if ($classification_id) {
        $query .= "
 INNER JOIN products
         ON bugs.product_id = products.id";
    }

    $query .= "
      WHERE bugs.creation_ts >= NOW() - " . $dbh->sql_interval('?', 'DAY') . "
        AND bugs.reporter = ?
        AND NOT (bugs.bug_status = 'RESOLVED' AND 
                 bugs.resolution IN ('DUPLICATE','INVALID','NOTABUG',
                                     'NOTGNOME','INCOMPLETE'))";
    
    push(@args, $days, $userid);
    
    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
       AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
       AND bugs.product_id = ?";
    }

    my $bugs = $dbh->selectcol_arrayref($query, undef, @args);

    my $buglist = "$urlbase/buglist.cgi?bug_id=" . join(",", @$bugs);

    return ($buglist);
}

sub get_patch_submitters_list {
    my($number, $days, $keyword, $links, $version,
       $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "      SELECT attachments.submitter_id AS userid, 
                              COUNT(DISTINCT attachments.attach_id) AS n 
                         FROM attachments ";
    if ($keyword || $version || $classification_id || $product_id) {
        $query .=" INNER JOIN bugs
                           ON attachments.bug_id = bugs.bug_id ";
    }
    if ($classification_id) {
        $query .= "
          INNER JOIN products
                  ON bugs.product_id = products.id";
    }
    $query .= "         WHERE attachments.creation_ts >= NOW() - " .
                                  $dbh->sql_interval('?', 'DAY') . "
                          AND attachments.ispatch = 1
                          AND attachments.isobsolete = 0";

    push(@args, $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "       AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "       AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
                 AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
       AND bugs.product_id = ?";
    }
    
    $query .= "      GROUP BY attachments.submitter_id
                     ORDER BY n DESC " . $dbh->sql_limit($number);

    my $submitterlist = $dbh->selectall_arrayref($query, undef, @args);

    foreach my $rowRef (@$submitterlist) {
        my ($userid, $count) = @$rowRef;

        push(@$rowRef, new Bugzilla::User($userid));
#        if ($links eq "yes") {
#            my $buglist = &get_reporter_bugs($userid, $days, $keyword, $version);
#            push(@$rowRef, $buglist);
#        }
    }

    return $submitterlist;
}

sub get_patch_reviewers_list {
    my($number, $days, $keyword, $links, $version,
       $classification_id, $product_id) = @_;

    my @args;

    # We are going to build a long SQL query.
    my $query = "
        SELECT bugs_activity.who AS userid, COUNT(DISTINCT bugs_activity.attach_id) as n 
          FROM bugs_activity ";
    if ($keyword || $version || $classification_id || $product_id) {
        $query .= "
    INNER JOIN bugs
            ON bugs_activity.bug_id = bugs.bug_id ";
    }
    if ($classification_id) {
        $query .= "
    INNER JOIN products
            ON bugs.product_id = products.id";
    }
    $query .= "
         WHERE bugs_activity.fieldid = ?
           AND bugs_activity.removed = 'none'
           AND bugs_activity.bug_when >= NOW() - " .
                   $dbh->sql_interval('?', 'DAY');

    push(@args, GetFieldID('flagtypes.name'), $days);

    if ($keyword) {
        push(@args, lc($keyword));
        $query .= "
           AND INSTR(LOWER(bugs.keywords), ?)";
    }
    if ($version) {
        push(@args, $version);
        $query .= "
           AND bugs.gnome_version = ?";
    }
    if ($classification_id) {
        push(@args, $classification_id);
        $query .= "
           AND products.classification_id = ?";
    }
    if ($product_id) {
        push(@args, $product_id);
        $query .= "
           AND bugs.product_id = ?";
    }

    $query .= "
      GROUP BY bugs_activity.who
      ORDER BY n desc " . $dbh->sql_limit($number);

    my $hunterlist = $dbh->selectall_arrayref($query, undef, @args);

    foreach my $rowRef (@$hunterlist) {
        my($userid, $count) = @$rowRef;
        
        push(@$rowRef, new Bugzilla::User($userid));
#        if ($links eq "yes") {
#            my $buglist = &get_hunter_bugs($userid, $days, $keyword, $version);
#            push(@$rowRef, $buglist);
#        }
    }

    return $hunterlist;
}

1;
