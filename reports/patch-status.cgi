#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
BEGIN {
    use Cwd;
    my $dir = cwd;
    if ($dir && $dir =~ /reports$/) {
        chdir "..";
    }
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
use lib ".";

use vars qw($vars);

require "globals.pl";
require "CGI.pl";

use Bugzilla;

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
Bugzilla->login();

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;

use vars qw(
    %components
    @legal_product
    @legal_components
);

#set up the page
GetVersionTable();

my $product = $cgi->param('product');
my $component = $cgi->param('component');

if(!defined($product) || $product eq '')
{
    my @products = @::legal_product;
    @products = sort { lc($a) cmp lc($b) } @products;
    $vars->{'product'} = \@products;
    

    # Sort the component list...
    my $comps = \%::components;
    foreach my $p (@products) {
        my @tmp = sort { lc($a) cmp lc($b) } @{$comps->{$p}};
        $comps->{$p} = \@tmp;
    }
    
    $vars->{'componentsbyproduct'} = $comps;
    $vars->{'component_'} = \@::legal_components;

    print "Content-type: text/html\n\n";    
    $template->process("reports/patch-status.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
} else {
  print "Content-type: text/html\n\n";
  CheckFormField($cgi, 'product',      \@::legal_product);
  my $quoted_product = url_quote($product);
  my $quoted_component = url_quote($component);
  my $quoted_urlbase = html_quote(Param("urlbase"));
  print "<HEAD>\n" .
        "<meta http-equiv=\"refresh\" content=\"0; " .
        "url=${quoted_urlbase}reports/patch-report.cgi?" .
        "product=$quoted_product&component=$quoted_component\">\n" .
        "</HEAD>\n";
}

