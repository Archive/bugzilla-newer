#!/usr/bin/perl -w
#
# Copyright 2004, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)
#
# Hacked up for 2.20 by Olav Vitters in 2005 and removed all the stuff I did
# not want.
#

use strict;

require "globals.pl";

use Bugzilla;

sub get_recent_mostfrequent_table {
    my ($days, $print_warnings) = @_;

    my $dbh = Bugzilla->dbh;

    #
    # First, get the fieldid of 'resolution'
    #
    my ($bug_status_fieldid) = GetFieldID('resolution');

    #
    # Now, the killer query to get all the bugs which are duplicates, with the
    # info of which bugs they are duplicates of
    #
    my $query = "
        SELECT duplicates.dupe_of, COUNT(bugs.bug_id)
          FROM bugs
    INNER JOIN bugs_activity
            ON bugs.bug_id = bugs_activity.bug_id
    INNER JOIN duplicates
            ON bugs.bug_id = duplicates.dupe
         WHERE bugs_activity.fieldid = ?
           AND bugs_activity.added = 'DUPLICATE'
           AND bugs_activity.bug_when >= NOW() - " .
                   $dbh->sql_interval('?', 'DAY') . "
      GROUP BY duplicates.dupe_of";

    my $the_data_yo = $dbh->selectall_arrayref($query, undef, 
                                               ($bug_status_fieldid, $days));

    #
    # Now, find bugs having duplicates which themselves are a duplicate.  For
    # those which are, change the bug# in @the_data_yo table to the bug# of
    # which they are a duplicate.  (Well, except that bug may be a duplicate too,
    # so follow the chain...)
    #

    my $sth_dupe = $dbh->prepare('
       SELECT dupe, dupe_of
         FROM duplicates
        WHERE dupe = ?');

    my $bugids = join (",", map { $_->[0] } @$the_data_yo);

    #
    # Return if there aren't any duplicates
    #

    if (!$bugids) {
        return $the_data_yo
    }

    $query = "
      SELECT bug_id, 1
        FROM bugs
       WHERE bug_id IN ($bugids)
         AND bug_status IN ('RESOLVED', 'VERIFIED', 'CLOSED')
         AND resolution = 'DUPLICATE'";

    my $dupe_is_dupe  = $dbh->selectall_hashref($query, 'bug_id');

    #
    # XXX - Return if none of the duplicates are closed as duplicate. ($dupe_is_dupe)
    #

    foreach my $listref (@$the_data_yo) {
        my @dupelist;
        my $is_dupe = 0;

        $is_dupe = defined($dupe_is_dupe->{$listref->[0]}) ? 1 : 0;    

        @dupelist = ($listref->[0]);
        while ($is_dupe) {
            $sth_dupe->execute($listref->[0]);
            my ($origbug, $newbug) = $sth_dupe->fetchrow_array;

            # Watch out for inconsistent database--do this before trying to use
            # $newbug
            if (!defined($newbug)) {
                if ($print_warnings) {
                    print "<!-- Warning: $listref->[0] is a dupe but has no entry in " .
                        "duplicates!! -->\n";
                }
                last;
            }

            # Check for circularity in duplicates
            if (grep {$_ == $newbug} @dupelist) {
                if ($print_warnings) {
                    print "<p><b><i>Warning!!!</b></i>" .
                        " Circular duplicates found in database: ";

                    foreach my $dupe (@dupelist) {
                        print "$dupe, ";
                    }
                    print "$newbug</p>\n";
                }

                last;
            }
            push @dupelist, $newbug;


            $listref->[0] = $newbug;
            $is_dupe = defined($dupe_is_dupe->{$listref->[0]}) ? 1 : 0;
        }
    }

    #
    # After the above, @the_data_yo can have multiple rows with the same bug#;
    # we want to combine those--we make a new table, @new_data, with this info
    #
    my @new_data;
    my ($cur_bug, $dupe_count) = (-1, 0, '');
    foreach my $listref (sort {$a->[0] <=> $b->[0]} @$the_data_yo) {
        if ($listref->[0] != $cur_bug) {
            if ($cur_bug != -1) {
                push @new_data, [$cur_bug, $dupe_count];
            }
            $cur_bug = $listref->[0];
            $dupe_count = $listref->[1];
        } else {
            $dupe_count += $listref->[1];
        }
    }
    push @new_data, [$cur_bug, $dupe_count];

    return \@new_data;
}

# required, don't remove
1;
