#!/usr/bin/perl -wT

# (c) Copyright Elijah Newren 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use strict;

require "CGI.pl";

require "globals.pl";

sub get_unreviewed_patches_and_stats {
    my ($quoted_product, $quoted_component, $patch_status,
        $min_days, $max_days, $submitter) = (@_);

    my $query;
    my $dbh = Bugzilla->dbh;
    
    $query = " SELECT attachments.attach_id, attachments.bug_id,
                      (" . $dbh->sql_to_days('NOW()') . "-" . 
                       $dbh->sql_to_days('attachments.creation_ts') . ") AS age,
                      substring(attachments.description, 1, 70),
                      products.name AS product, components.name AS component
                 FROM attachments
           INNER JOIN bugs
                   ON attachments.bug_id = bugs.bug_id
           INNER JOIN products
                   ON bugs.product_id = products.id
           INNER JOIN components
                   ON bugs.component_id = components.id
                WHERE attachments.ispatch = '1'";

    if ($quoted_product && $quoted_product ne "'%'") {
      trick_taint($quoted_product);
      $query .= " AND products.name IN ($quoted_product)";
    }
    if ($quoted_component && $quoted_component ne "'%'") {
      trick_taint($quoted_component);
      $query .= " AND components.name IN ($quoted_component)";
    }
    if ($submitter) {
      # $submitter is a numeric
      $query .= " AND attachments.submitter_id = $submitter";
    }
    if ($min_days && $min_days != -1) {
      $query .= " AND attachments.creation_ts <= NOW() - " .
          $dbh->sql_interval($min_days, 'DAY');
    }
    if ($max_days && $max_days != -1) {
      $query .= " AND attachments.creation_ts >= NOW() - " .
          $dbh->sql_interval($max_days, 'DAY');
    }
    if ($patch_status eq 'obsolete') {
      $query .= " AND attachments.isobsolete  = '1'";
    } else {
      $query .= " AND attachments.isobsolete != '1'";
    }
    if ($patch_status eq 'none') {
      $query .= " AND attachments.status_id = 0";
    } elsif ($patch_status ne 'obsolete') {
      my ($id) = GetStatusID($patch_status);
      $query .= " AND attachments.status_id = " . $dbh->quote($id);
    }
    $query .= "   AND (bugs.bug_status = 'UNCONFIRMED'
                       OR bugs.bug_status = 'NEW'
                       OR bugs.bug_status = 'ASSIGNED'
                       OR bugs.bug_status = 'REOPENED')
             ORDER BY products.name, components.name, attachments.bug_id, attachments.attach_id";

    my $sth = $dbh->prepare($query);
    $sth->execute();

    $query = "SELECT substring(short_desc, 1, 70), priority, bug_severity
                FROM bugs
               WHERE bug_id = ?";

    my $sth_Buginfo = $dbh->prepare($query);

    my ($cur_product, $cur_component, $cur_bug) = ('', '', 0);
    my ($prod_list, $comp_list, $bug_list, $patch_list) = ([], [], [], []);
    my ($new_product, $new_component);
    my ($total_count, $prod_count, $comp_count) = (0, 0, 0);
    my $stats = {
        'count' => 0,
        'product_list' => []
    };
    
    $prod_list = $stats->{product_list};  
    while (my ($attach_id, $bug_id, $age, $desc, $prod, $comp) = $sth->fetchrow_array) {

        # Check if we've moved on to a new product
        if ($cur_product ne $prod) {
            if ($cur_product ne '') {
                $new_product->{count} = $prod_count;
                $prod_count = 0;
                $new_component->{count} = $comp_count;
                $comp_count = 0;
            }
            
            $cur_product = $prod;
            $new_product = {
                'name' => $prod,
                'component_list' => []
            };
            
            push @{$prod_list}, $new_product;
            $cur_component = '';
            $comp_list = $new_product->{component_list};
        }
        
        # Check if we've moved on to a new component
        if ($cur_component ne $comp) {
            if ($cur_component ne '') {
                $new_component->{count} = $comp_count;
                $comp_count = 0;
            }
            $cur_component = $comp;

            $new_component = {
                'name' => $comp,
                'bug_list' => []
            };
            push @{$comp_list}, $new_component;
            $cur_bug = 0;
            $bug_list = $new_component->{bug_list};
        }
        
        # Check if we've moved on to a new bug
        if ($cur_bug ne $bug_id) {
            $cur_bug = $bug_id;

            $sth_Buginfo->execute($bug_id);
            my ($bug_desc, $priority, $severity) = $sth_Buginfo->fetchrow_array;

            my $new_bug = {
                'id' => $bug_id,
                'summary' => $bug_desc,
                'priority' => $priority,
                'severity' => $severity,
                'patch_list' => []
            };
            push @{$bug_list}, $new_bug;
            $patch_list = $new_bug->{patch_list};
        }

        my $new_patch = {
            'id' => $attach_id,
            'age' => $age,
            'description' => $desc
        };
        push @{$patch_list}, $new_patch;

        $total_count++;
        $prod_count++;
        $comp_count++;

        # printf "%6d %6d %s %s %s\n", $attach_id, $bug_id, $prod, $comp, $desc;
    }

    # Update the counts for the final product and component, as well as the total
    $stats->{count} = $total_count;
    $new_product->{count} = $prod_count;
    $new_component->{count} = $comp_count;

    return $stats;
}

# Let perl know we ended okay
1;
