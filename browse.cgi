#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>
#                 Myk Melez <myk@mozilla.org>
#                 Gervase Markham <gerv@gerv.net>

use strict;

use lib qw(.);
use vars qw ($template $vars);
# Suppress "used only once" warnings.
use vars 
  qw(
    %proddesc
    %classdesc
  );

use Bugzilla;
use Bugzilla::Search qw(SqlifyDate);
use Bugzilla::Constants;
use SortVersions;

require "CGI.pl";

use vars qw($vars @legal_product @legal_gnome_target @legal_priority 
            @legal_severity);

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;

###############################################################################
# Begin Data/Security Validation
###############################################################################

# Check whether or not the user is currently logged in. 
Bugzilla->login();
GetVersionTable();

# Make sure the product is authorized to be access by the user.
my $product = trim($cgi->param('product') || '');

if (!$product && $cgi->cookie('DEFAULTPRODUCT')
    && CanEnterProduct($cgi->cookie('DEFAULTPRODUCT')))
{
    $product = $cgi->cookie('DEFAULTPRODUCT');
}

my $product_id = get_product_id($product);
my $product_interests = Bugzilla->user->product_interests();

# If the user didn't select a product and there isn't a default from a cookie,
# try getting the first valid product from their interest list.
if (!$product_id && scalar @$product_interests) {
  foreach my $try (@$product_interests) {
    if (CanEnterProduct($try)) {
      $product = $try;
      $product_id = get_product_id($product);
      last;
    }
  }
}


if (!$product_id || !CanEnterProduct($product)) {
    # FIXME: I suck for not handling stuff like
    # Param('useclassification') being false, and not checking for there
    # only begin one product in the result, and maybe other stuff

    # Create data structures representing each classification
    my @classifications = ();
    if (scalar @$product_interests) {
        my %watches = (
            'name'     => 'Products you watch',
            'products' => $product_interests
        );
        push @classifications, \%watches;
    }
    if (Param('useclassification')) {
        foreach my $c (GetSelectableClassifications()) {
            # Create hash to hold attributes for each classification.
            my %classification = (
                'name'       => $c,
                'products'   => [ GetEnterableProducts($c) ]
            );
            # Assign hash back to classification array.
            push @classifications, \%classification;
        }
    }

    $vars->{'classifications'} = \@classifications;
    $vars->{'classdesc'} = \%::classdesc;
    $vars->{'proddesc'} = \%::proddesc;

    $vars->{'target'} = "browse.cgi";
    $vars->{'format'} = $cgi->param('format');

    print $cgi->header();
    $template->process("global/choose-product-flatlist.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
    exit;        
}

# Remember selected product
$cgi->send_cookie(-name => 'DEFAULTPRODUCT',
                  -value => $product,
                  -expires => "Fri, 01-Jan-2038 00:00:00 GMT");


###############################################################################
# End Data/Security Validation
###############################################################################

#($vars->{'operations'}, $vars->{'incomplete_data'}) = 
#                                                 GetBugActivity($bug_id);

# Create data structures representing each classification
my @classifications = ();
if (scalar @$product_interests) {
    my %watches = (
        'name'     => 'Watched Products',
        'products' => $product_interests
    );
    push @classifications, \%watches;
}
if (Param('useclassification')) {
    foreach my $c (GetSelectableClassifications()) {
        # Create hash to hold attributes for each classification.
        my %classification = (
            'name'       => $c,
            'products'   => [ GetSelectableProducts(0,$c) ]
        );
        # Assign hash back to classification array.
        push @classifications, \%classification;
    }
}
$vars->{'classification'} = \@classifications;

# For usage in SQL
my $open_states = join(",",map { $dbh->quote($_) } grep { $_ ne "NEEDINFO" } OpenStates() );
my $important_patch_statuses = "'none'," .
                               "'accepted-commit_now'," .
                               "'accepted-commit_after_freeze'";

$vars->{'open_states'} = $open_states;
$vars->{'product'} = $product;
$vars->{'description'} = $::proddesc{$product};
$vars->{'product_homepage'} = $dbh->selectrow_array("SELECT milestoneurl FROM products WHERE id = ?", undef, $product_id);

$vars->{'total_open_bugs'} = $dbh->selectrow_array("SELECT COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) and product_id = ?", undef, $product_id);

$vars->{'what_new_means'} = $dbh->selectrow_array("SELECT NOW() - " . $dbh->sql_interval(7, 'DAY'));

$vars->{'new_bugs'} = $dbh->selectrow_array("SELECT COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) and creation_ts >= NOW() - " . $dbh->sql_interval(7, 'DAY') . " and product_id = ?", undef, $product_id);

$vars->{'new_patches'} = $dbh->selectrow_array("SELECT COUNT(attach_id) FROM bugs, attachments WHERE bugs.bug_id = attachments.bug_id AND bug_status IN ($open_states) AND attachments.ispatch = '1' AND attachments.isobsolete = '0' AND attachments.status_id = '0' AND attachments.creation_ts >= NOW() - " . $dbh->sql_interval(7, 'DAY') . " and product_id = ?", undef, $product_id);

$vars->{'gnome_love_bugs'} = $dbh->selectrow_array("SELECT COUNT(bugs.bug_id) FROM bugs, keywords WHERE bugs.bug_id = keywords.bug_id AND bug_status IN ($open_states) AND keywords.keywordid = ? AND product_id = ?", undef, GetKeywordIdFromName('gnome-love'), $product_id);

## Strangely, this next line doesn't seem to work, I guess due to the count
## in both the select and having clauses.  An alternate query has to be used
## below
#$vars->{'no_response_bugs'} = $dbh->selectrow_array("SELECT COUNT(bugs.bug_id) FROM bugs INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id WHERE bug_status IN ($open_states) AND bug_severity != 'enhancement' AND product_id = ? GROUP BY bugs.bug_id HAVING count(distinct longdescs.who) = '1'", undef, $product_id);

$vars->{'no_response_bugs'} = scalar @{$dbh->selectall_arrayref("SELECT bugs.bug_id FROM bugs INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id WHERE bug_status IN ($open_states) AND bug_severity != 'enhancement' AND product_id = ? AND bugs.reporter NOT IN (SELECT userid FROM developers WHERE product_id = ?) GROUP BY bugs.bug_id HAVING count(distinct longdescs.who) = '1'", undef, $product_id, $product_id)};

######################################################################
# Begin temporary searches; If the search will be reused again next
# release cycle, please just comment it out instead of deleting it.
######################################################################
$vars->{'critical_warning_bugs'} = $dbh->selectrow_array("SELECT COUNT(bugs.bug_id) FROM bugs INNER JOIN longdescs ON longdescs.bug_id = bugs.bug_id WHERE bug_status IN ($open_states) AND " . $dbh->sql_fulltext_search("longdescs.thetext", "'+G_LOG_LEVEL_CRITICAL'") . " AND product_id = ?", undef, $product_id);

#$vars->{'string_bugs'} =  $dbh->selectrow_array("SELECT COUNT(bugs.bug_id) FROM bugs, keywords, keyworddefs WHERE bugs.bug_id = keywords.bug_id AND keywords.keywordid = keyworddefs.id AND keyworddefs.name = 'string' AND bug_status IN ($open_states) AND product_id = ?", undef, $product_id);
######################################################################
# End temporary searches
######################################################################


$vars->{'by_patch_status'} = $dbh->selectall_arrayref("SELECT flagtypes.name, COUNT(attach_id) FROM bugs, attachments, flagtypes WHERE attachments.bug_id = bugs.bug_id AND flagtypes.id = attachments.status_id AND bug_status IN ($open_states) AND product_id = ? AND attachments.ispatch = '1' AND attachments.isobsolete != '1' AND flagtypes.name IN ($important_patch_statuses) GROUP BY flagtypes.name", undef, $product_id);

$vars->{'buglink'} = Param('urlbase') . 'buglist.cgi?product=' . url_quote($product) . join ('', map { '&bug_status=' . url_quote($_) } grep { $_ ne "NEEDINFO" } OpenStates() );

my @by_version = sort { versioncmp($a->[0], $b->[0]) } @{$dbh->selectall_arrayref("SELECT version, COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) and product_id = ? GROUP BY version", undef, $product_id)};
$vars->{'by_version'} = \@by_version;


my $ni_a = Bugzilla::Search::SqlifyDate('-2w');
my $ni_b = Bugzilla::Search::SqlifyDate('-4w');
my $ni_c = Bugzilla::Search::SqlifyDate('-3m');
my $ni_d = Bugzilla::Search::SqlifyDate('-6m');
my $ni_e = Bugzilla::Search::SqlifyDate('-1y');
my $needinfo_case = "CASE WHEN delta_ts < '$ni_e' THEN 'F'
                          WHEN delta_ts < '$ni_d' THEN 'E'
                          WHEN delta_ts < '$ni_c' THEN 'D'
                          WHEN delta_ts < '$ni_b' THEN 'C'
                          WHEN delta_ts < '$ni_a' THEN 'B'
                          ELSE 'A' END";
my %needinfo_split = @{$dbh->selectcol_arrayref("SELECT $needinfo_case age, COUNT(bug_id) FROM bugs WHERE bug_status = 'NEEDINFO' AND product_id = ? GROUP BY $needinfo_case", { Columns=>[1,2] }, $product_id)};
$vars->{'needinfo_split'} = \%needinfo_split;

my @by_target = sort { versioncmp($a->[0], $b->[0]) } @{$dbh->selectall_arrayref("SELECT target_milestone, COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) and target_milestone != '---' AND product_id = ? GROUP BY target_milestone", undef, $product_id)};
$vars->{'by_target'} = \@by_target;

my $i = 0;
my %order_priority = map { $_ => $i++  } @::legal_priority;
my @by_priority = sort { $order_priority{$a->[0]} <=> $order_priority{$b->[0]} } @{$dbh->selectall_arrayref("SELECT priority, COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) AND product_id = ? GROUP BY priority", undef, $product_id)};
$vars->{'by_priority'} = \@by_priority;

$i = 0;
my %order_severity = map { $_ => $i++  } @::legal_severity;

my @by_severity = sort { $order_severity{$a->[0]} <=> $order_severity{$b->[0]} } @{$dbh->selectall_arrayref("SELECT bug_severity, COUNT(bug_id) FROM bugs WHERE bug_status IN ($open_states) AND product_id = ? GROUP BY bug_severity", undef, $product_id)};
$vars->{'by_severity'} = \@by_severity;

$vars->{'by_component'} = $dbh->selectall_arrayref("SELECT components.name, COUNT(bugs.bug_id) FROM bugs INNER JOIN components ON bugs.component_id = components.id WHERE bug_status IN ($open_states) AND bugs.product_id = ? GROUP BY components.name", undef, $product_id);

#$vars->{'by_assignee'} = map(new Bugzilla::User($_), $dbh->selectall_arrayref("SELECT bugs.assignee AS userid, COUNT(bugs.bug_id) FROM bugs HERE bug_status IN ($open_states) AND bugs.product_id = ? GROUP BY components.name", undef, $product_id);


my $gnome_target_a = $::legal_gnome_target[(scalar @::legal_gnome_target) -1];
my $gnome_target_b = $::legal_gnome_target[(scalar @::legal_gnome_target) -2];

$vars->{'target_development'} = $gnome_target_a;
$vars->{'target_stable'} = $gnome_target_b;

my $sth = $dbh->prepare("SELECT bugs.bug_id, products.name AS product, bugs.bug_status, bugs.resolution, bugs.bug_severity, bugs.short_desc FROM bugs INNER JOIN products ON bugs.product_id = products.id WHERE product_id=? AND bugs.gnome_target = ? AND bug_status IN ($open_states) ORDER BY bug_id DESC");

my @list_blockers_development;
$sth->execute($product_id, $gnome_target_a);
while (my $bug = $sth->fetchrow_hashref) {
    push(@list_blockers_development, $bug);
}
$vars->{'blockers_development'} = \@list_blockers_development;

my @list_blockers_stable;
$sth->execute($product_id, $gnome_target_b);
while (my $bug = $sth->fetchrow_hashref) {
    push(@list_blockers_stable, $bug);
}
$vars->{'blockers_stable'} = \@list_blockers_stable;


my $query = "SELECT login_name AS email, realname AS name
             FROM profiles, developers
             WHERE profiles.userid = developers.userid
               AND developers.product_id = $product_id";
$vars->{'developers'} = $dbh->selectall_arrayref($query);
$vars->{'canedit'} = UserHasPermsToEditProduct($product_id);

print Bugzilla->cgi->header();

$template->process("browse/main.html.tmpl", $vars)
  || ThrowTemplateError($template->error());

