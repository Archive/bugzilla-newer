#!/usr/bin/perl -wT
# 
# simple-bug-guide.cgi
# This page is linked to from within GNOME and should *always* be accessible
# from http://bugzilla.gnome.org/simple-bug-guide.cgi
# 
# It takes these arguments. It's OK to call it with no arguments or any
# combination of the below; it tries to Do The Right Thing
# 
# sbg_type: what type of bug we're filing. This is used to determine what sort of
# template to use for the bug entry, and also affects what questions the user
# gets asked to determine values for other unset arguments. Valid values are:
#   normal
#   crasher
#   usability
#   docs
#   enhancement
#   l10n (translation issues)
# It's case sensitive. Deal with it.
# 
# product
# application - exactly the same as product unless you're filing an l10n bug.
# comp
# severity
# priority
# version
# gnome_version (GNOME Version as per the customfield)
# 
# Copyright (c) Andrew Sobala 2004.
# Licensed under the GNU General Public License Version 2 and the Mozilla
# Public License.

use lib qw(.);
use strict;
require "CGI.pl";

use Bugzilla;
use Bugzilla::Constants;
use Bugzilla::Token;

GetVersionTable ();

my $dbh = Bugzilla->dbh;
my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;

use vars qw (
    $vars
    %versions
    $sbg_type
    $product
    $application
    $comp
    $severity
    $priority
    $version
    $gnome_version
    @legal_gnome_version
    %proddesc
    %classdesc
    %classifications
    @enterable_products
);

$sbg_type = $cgi->param('sbg_type');
$product = $cgi->param('product');
$application = $cgi->param('application');
my $classification = $cgi->param('classification');

$comp = $cgi->param('comp');
$severity = $cgi->param('severity');
$priority = $cgi->param('priority');
$version = $cgi->param('version');
$gnome_version = $cgi->param('gnome_version');

# Some standard vars
$vars->{'format'} = $cgi->param('format');

sub copy_to_vars ()
{
    $vars->{'sbg_type'} = $sbg_type;
    $vars->{'product'} = $product;
    $vars->{'application'} = $application;
    $vars->{'comp'} = $comp;
    $vars->{'severity'} = $severity;
    $vars->{'priority'} = $priority;
    $vars->{'version'} = $version;
    $vars->{'gnome_version'} = $gnome_version;
    $vars->{'classification'} = $classification;
}

##
# A bit straight from enter_bug.cgi

my %products;

foreach my $p (@::enterable_products) {
    $products{$p} = $::proddesc{$p};
}
my $prodsize = scalar(keys %products);
if ($prodsize == 0) {
    ThrowUserError("no_products");
} 
elsif ($prodsize > 1) {
    $vars->{'proddesc'} = \%products;
    $vars->{'classdesc'} = \%classdesc;
    $vars->{'classifications'} = \%classifications;
    $vars->{'classification'} = $classification;
}

# Let's go.
# Main application logic here :)

Bugzilla->login(LOGIN_REQUIRED);

#### TYPE
if (!defined ($sbg_type)) {
    if ($severity && $severity eq "enhancement") {
        $sbg_type = "enhancement";
    }
    if ($product && $product eq "l10n") {
        $sbg_type = "l10n";
    }
}

if (!defined ($sbg_type)) {
    my $format = GetFormat("bug/sbg/select-type", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));

    copy_to_vars ();
    print $cgi->header($format->{'ctype'});
    $template->process("$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit();
}

#### APPLICATION AND PRODUCT
# "calling this script after defining product and application to be set but
# different is unsupported, unless you're sure what you're doing is right"
# or something
# (this is real voodoo)

if (defined ($product) && !defined($application) && $product ne "l10n") {
    $application = $product;
}
if (defined ($application) && !defined ($product)) {
    $product = $application;
}
$classification ||= 'Desktop';

if ($sbg_type eq "l10n") {
    $product = "l10n";
}

if (!defined ($application) || $cgi->param('forceclassification')) {
    my $format = GetFormat("bug/sbg/select-product", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));

    copy_to_vars ();
    print $cgi->header($format->{'ctype'});
    $template->process("$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}

CanEnterProductOrWarn($product);

my $product_id = get_product_id($product);

#### COMPONENT
if (!defined ($comp)) {
# from enter_bug.cgi
    if (1 == @{$::components{$product}}) {
        # Only one component; just pick it.
        $comp = $::components{$product}->[0];
    }
    elsif (($sbg_type eq "docs") &&
           (my @doccomps = grep(lc($_) eq "docs", @{$::components{$product}}))) {
        # For documentation bugs automatically select docs component
        # Note: Not copy pasted from enter_bug.cgi
        $comp = $doccomps[0];
    }

    my @components;
    
    # Retrieve the descriptions for every component but the 'docs'.
    # The docs component would already be selected above
    # Note: Not selecting 'docs' is not from enter_bug.cgi
    my $dbh = Bugzilla->dbh;
    my $sth = $dbh->prepare(
           q{SELECT name, description, p1.login_name, p2.login_name
               FROM components
          LEFT JOIN profiles p1 ON components.initialowner = p1.userid
          LEFT JOIN profiles p2 ON components.initialqacontact = p2.userid
              WHERE product_id = ? AND name != 'docs'
              ORDER BY name});

    $sth->execute($product_id);
    while (my ($name, $description, $owner, $qacontact)
           = $sth->fetchrow_array()) {
        push @components, {
            name => $name,
            description => $description,
            initialowner => $owner,
            initialqacontact => $qacontact || '',
        };
    }
# end enter_bug.cgi
    
    # It is possible that there are only two components, and one of them
    # is 'docs'. In that case we currently only have one component.
    # If so, select that one
    if (!defined($comp) && (0 == $#components)) {
        $comp = $components[0]{'name'};
    }
    
    $vars->{'components'} = \@components;

    if (!defined ($comp)) {
        my $format = GetFormat("bug/sbg/select-comp", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));

        copy_to_vars ();
        print $cgi->header($format->{'ctype'});
        $template->process("$format->{'template'}", $vars)
            || ThrowTemplateError($template->error());
        exit ();
    }
}

#### SEVERITY AND PRIORITY
if (!defined ($severity)) {
    if ($sbg_type eq "enhancement") {
        $severity = "enhancement";
    }
    if ($sbg_type eq "crasher") {
        $severity = "critical";
        if (!defined ($priority)) {
            $priority = "High";
        }
    }
    if ($sbg_type eq "usability" || $sbg_type eq "docs") {
        $severity = "minor";
    }
    if ($sbg_type eq "l10n") {
        $severity = "normal";
    }
}
if (!defined ($priority)) {
    $priority = "Normal";
}
if (!defined ($severity)) {
    my $format = GetFormat("bug/sbg/select-severity", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));

    copy_to_vars ();
    print $cgi->header($format->{'ctype'});
    $template->process("$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}


#### VERSION AND GNOME_VERSION
if ($sbg_type eq "enhancement") {
    $gnome_version = "Unversioned Enhancement";
}
# Determine if product is a GNOME product using the classification
$classification = $dbh->selectrow_array(
        'SELECT classifications.name ' .
         'FROM products ' .
   'INNER JOIN classifications ' .
           'ON products.classification_id = classifications.id ' .
        'WHERE products.id = ?', undef, $product_id);

if (!&IsGnomeClassification($classification)) {
    $gnome_version = "Unspecified";
}
if (1 == @{$::versions{$product}}) {
    # Only one version; just pick it.
    $version = $::versions{$product}->[0];
}
if (!defined ($version) || !defined ($gnome_version)) {
    $vars->{'versions'} = $::versions{$product};
    $vars->{'gnome_versions'} = \@::legal_gnome_version;

    my $format = GetFormat("bug/sbg/select-version", scalar $cgi->param('format'),
                       scalar $cgi->param('ctype'));

    copy_to_vars ();
    print $cgi->header($format->{'ctype'});
    $template->process("$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}

#### BUG_TEXT
# Set keywords automatically
    if ($sbg_type eq "usability") {
        $vars->{'keywords'} = "usability";
    }
    if ($sbg_type eq "docs") {
        $vars->{'keywords'} = "documentation";
    }
    if ($sbg_type eq "l10n") {
        $vars->{'keywords'} = "L10N";
    }
# we override format to be sbg_type for this last bit
    $vars->{'token'} = Bugzilla::Token::IssueSessionToken('createbug:');
    my $format = GetFormat("bug/sbg/enter-bug", $sbg_type,
                       scalar $cgi->param('ctype'));

    copy_to_vars ();
    print $cgi->header($format->{'ctype'});
    $template->process("$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();

