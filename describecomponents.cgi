#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>
#                 Bradley Baetz <bbaetz@student.usyd.edu.au>

use strict;
use lib qw(.);

use Bugzilla;
use Bugzilla::Constants;
require "CGI.pl";

use vars qw($vars @legal_product);
# Suppress "used only once" warnings.
use vars 
  qw(
    %proddesc
    %classdesc
  );

Bugzilla->login();

GetVersionTable();

my $cgi = Bugzilla->cgi;
my $template = Bugzilla->template;
my $product = trim($cgi->param('product') || '');
my $product_id = get_product_id($product);

if (!$product_id || !CanEnterProduct($product)) {
    # FIXME: I suck for not handling stuff like
    # Param('useclassification') being false, and not checking for there
    # only begin one product in the result, and maybe other stuff

    # Create data structures representing each classification
    my @classifications = ();
    my $product_interests = Bugzilla->user->product_interests();
    if (scalar @$product_interests) {
        my %watches = (
            'name'     => 'Products you watch',
            'products' => $product_interests
        );
        push @classifications, \%watches;
    }
    if (Param('useclassification')) {
        foreach my $c (GetSelectableClassifications()) {
            # Create hash to hold attributes for each classification.
            my %classification = (
                'name'       => $c,
                'products'   => [ GetEnterableProducts($c) ]
            );
            # Assign hash back to classification array.
            push @classifications, \%classification;
        }
    }

    $vars->{'classifications'} = \@classifications;
    $vars->{'classdesc'} = \%::classdesc;
    $vars->{'proddesc'} = \%::proddesc;

    $vars->{'target'} = "describecomponents.cgi";
    $vars->{'format'} = $cgi->param('format');

    print $cgi->header();
    $template->process("global/choose-product-flatlist.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
    exit;        
}

######################################################################
# End Data/Security Validation
######################################################################

my @components;
SendSQL("SELECT name, initialowner, initialqacontact, description FROM " .
        "components WHERE product_id = $product_id ORDER BY name");
while (MoreSQLData()) {
    my ($name, $initialowner, $initialqacontact, $description) =
      FetchSQLData();

    my %component;

    $component{'name'} = $name;
    $component{'description'} = $description;

    my $dbh = Bugzilla->dbh;
    my $sth = $dbh->prepare(
            "SELECT realname AS name, login_name AS email
             FROM   profiles
            WHERE   profiles.userid = ?");

    # We want the template to show the email address if the user is logged
    # in, and to show the name otherwise.
    if ($initialowner) {
      $sth->execute($initialowner);
      my $owner_ref = $sth->fetchrow_hashref();
      my %owner = %$owner_ref;
      $owner{'email'} .= Param('emailsuffix');
      $owner{'name'} = $owner{'name'} || $owner{'email'};
      $component{'initialowner'} = \%owner;
    } else {
      $component{'initialowner'} = { name => '', email => '' };
    }

    if ($initialqacontact) {
      $sth->execute($initialqacontact);
      my $qacontact_ref = $sth->fetchrow_hashref();
      my %qacontact = %$qacontact_ref;
      # Param('emailsuffix') is appended in the template
      $qacontact{'name'} = $qacontact{'name'} || $qacontact{'email'};
      $component{'initialqacontact'} = \%qacontact;
    } else {
      $component{'initialqacontact'} = { name => '', email => '' };
    }

    push @components, \%component;
}

$vars->{'product'} = $product;
$vars->{'components'} = \@components;

print $cgi->header();
$template->process("reports/components.html.tmpl", $vars)
  || ThrowTemplateError($template->error());
