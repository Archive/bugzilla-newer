#!/usr/bin/perl -wT
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Terry Weissman <terry@mozilla.org>

use strict;

use lib qw(.);
use vars qw ($template $vars);
# Suppress "used only once" warnings.
use vars 
  qw(
    %proddesc
    %classdesc
  );

use Bugzilla;
use Bugzilla::Constants;
use Bugzilla::GNOME;
use Digest::MD5 qw(md5_hex);

require "CGI.pl";

use vars qw($vars @legal_product @legal_gnome_target @legal_gnome_version
            @legal_priority @legal_severity);

my $cgi = Bugzilla->cgi;
my $dbh = Bugzilla->dbh;

###############################################################################
# Begin Data/Security Validation
###############################################################################

# Check whether or not the user is currently logged in. 
Bugzilla->login(LOGIN_REQUIRED);
GetVersionTable();

UserInGroup("edittraces")
  || ThrowUserError("auth_failure", {group  => "edittraces",
                                     action => "edit",
                                     object => "traces"});

my $action   = trim($cgi->param('action')   || '');
my $hash     = trim($cgi->param('hash')     || '');
my $old_hash = trim($cgi->param('old_hash') || '');
trick_taint($hash); trick_taint($old_hash);

my $report = "traces/list.html.tmpl";
my $cur_trace;

print $cgi->header();

sub ValidateForm {
    my %cur_trace;

    my $id = trim($cgi->param('dupe_of') || '');
    ValidateBugID($id);

    my $trace = trim($cgi->param('trace') || '');
    $trace =~ s/\r\n?/\n/g;
    trick_taint($trace);
    my $reason = trim($cgi->param('reason') || undef);
    trick_taint($reason) if defined $reason;
    my $version = trim($cgi->param('version') || '');
    trick_taint($version);
    $version = undef unless $version;
    
    my $product_id = trim($cgi->param('product_id') || undef);

    my $product;
    if ($product_id) {
        detaint_natural($product_id) || ThrowCodeError("invalid_product_id");
        $product = get_product_name($product_id);
        CanEnterProductOrWarn($product);
    }
   
    CheckFormField($cgi, 'gnome_version', \@legal_gnome_version)
        if $cgi->param('gnome_version');
    my $gnome_version = $cgi->param('gnome_version');
    $gnome_version = undef unless $gnome_version;
    trick_taint($gnome_version) if $gnome_version;

    

    my $functions = join(' ', get_traces_from_string($trace));
    my $hash = md5_hex($functions);

    %cur_trace = (
        hash => $hash,
        dupe_of => $id,
        product_id => $product_id,
        user_id => Bugzilla->user->id,
        version => $version,
        gnome_version => $gnome_version,
        trace => $trace,
        reason => $reason,

        functions => $functions,
        product => $product,
    );

    return \%cur_trace;
}

if ($action eq 'add') {
    $report = "traces/show.html.tmpl";

    $cur_trace = ValidateForm();

    $cur_trace->{'is_dupe'} = $dbh->selectrow_array(
        'SELECT 1 FROM traces WHERE hash = ?', undef, $cur_trace->{'hash'});

    if ($cgi->param('do_add') && $cur_trace->{'functions'} ne ''
        && !$cur_trace->{'is_dupe'})
    {
        $dbh->do('INSERT INTO traces (hash, dupe_of, product_id, user_id,
                                      version, gnome_version, trace, reason)
                       VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
               undef, ($cur_trace->{'hash'}, $cur_trace->{'dupe_of'},
                       $cur_trace->{'product_id'}, $cur_trace->{'user_id'},
                       $cur_trace->{'version'}, $cur_trace->{'gnome_version'},
                       $cur_trace->{'trace'}, $cur_trace->{'reason'}));
        
        $vars->{'message'} = 'trace_created';
        $action = 'edit';
        $old_hash = '';
    }
    else {
        $vars->{'cur_trace'} = $cur_trace;
    }
}
if ($action eq 'edit') {
    $report = "traces/show.html.tmpl";

    if ($old_hash) {
        # Validate:
        $cur_trace = ValidateForm();
        my $trace_info = Bugzilla::GNOME->get_traces_info($old_hash);
        ThrowUserError('trace_nonexistent') unless @{$trace_info};

        $cur_trace->{'old_hash'} = $old_hash;

        if ($cur_trace->{'old_hash'} eq $cur_trace->{'hash'}) {
            $cur_trace->{'is_dupe'} = 0
        } else {
          $cur_trace->{'is_dupe'} = $dbh->selectrow_array(
                'SELECT 1 FROM traces WHERE hash = ?', undef, $cur_trace->{'hash'});
        }

        if ($cgi->param('do_edit') && $cur_trace->{'functions'} ne ''
            && !$cur_trace->{'is_dupe'})
        {
            $dbh->do('UPDATE traces
                         SET hash = ?, dupe_of = ?, product_id = ?, user_id = ?,
                             version = ?, gnome_version = ?, trace = ?, 
                             reason = ?
                       WHERE hash = ?',
                     undef, ($cur_trace->{'hash'}, $cur_trace->{'dupe_of'},
                             $cur_trace->{'product_id'}, $cur_trace->{'user_id'},
                             $cur_trace->{'version'}, $cur_trace->{'gnome_version'},
                             $cur_trace->{'trace'}, $cur_trace->{'reason'},
                             $cur_trace->{'old_hash'}));

            # Very important (old_hash has been changed in the database to hash!):
            $cur_trace->{'old_hash'} = $cur_trace->{'hash'};

            $trace_info = Bugzilla::GNOME->get_traces_info($cur_trace->{'hash'});
            $cur_trace = $trace_info->[0]; # get the data again from the database.. just for testing purposes
            $vars->{'message'} = 'trace_updated';
        }

    } else {
        my $trace_info = Bugzilla::GNOME->get_traces_info($hash);
        ThrowUserError('trace_nonexistent') unless @{$trace_info};

        $cur_trace = $trace_info->[0];
        $cur_trace->{'old_hash'} = $cur_trace->{'hash'};
    }
}
elsif ($action eq "delete") {
    my $trace_info = Bugzilla::GNOME->get_traces_info($hash);
    ThrowUserError('trace_nonexistent') unless @{$trace_info};

    $cur_trace = $trace_info->[0];
    
    if ($cgi->param('do_delete')) {
        $dbh->do('DELETE FROM traces WHERE hash = ?', undef, ($hash));
        $vars->{'message'} = 'trace_deleted';
    }
    else {
        $action = 'delete';
        $report = 'traces/confirm-delete.html.tmpl';
    }
}

$vars->{'legal_gnome_versions'} = \@legal_gnome_version;
my %products = reverse GetSelectableProducts(1);
$vars->{'products'} = \%products;
$vars->{'action'} = $action;

if ($cur_trace && %{$cur_trace}) {
    if (defined $cur_trace->{'dupe_of'}) {
        $cur_trace->{'dupe_of_bug'} =
            new Bugzilla::Bug($cur_trace->{'dupe_of'}, Bugzilla->user->id);
    }

    if (!defined $cur_trace->{'functions'}
        && defined $cur_trace->{'trace'})
    {
        $cur_trace->{'functions'} =
            join(' ', get_traces_from_string($cur_trace->{'trace'}));
    }
    $vars->{'cur_trace'} = $cur_trace;
}

$template->process($report, $vars)
  || ThrowTemplateError($template->error());


